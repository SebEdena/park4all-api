# park4all-api
API for the park4all project path computation.

## Windows setup
### Prerequisites
- [Git CLI (bash)](https://git-scm.com/) latest release
- [Mapbox Account](https://www.mapbox.com/) with an access token
- [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

### Setup
On the destination folder in CLI, run `git clone https://gitlab.com/SebEdena/park4all-api.git`.

Open IntelliJ and open the folder the project park4all-api.

Open `File / Settings / Plugins / Marketplace` and add the plugin `Lombok`.

On maven window, run the step `Install`.

Duplicate file `secret.properties.txt` and rename copy as `secret.properties`. Insert your Mapbox access token to replace the string `<Your Mapbox Token here>` for the key `mapbox.accessToken` and save your modifications.

Run the class `Park4allApiApplication`.

If you see this log : 
`INFO 20756 --- [           main] c.p.park4allapi.Park4allApiApplication   : Started Park4allApiApplication in 8.946 seconds (JVM running for 11.552)`
it means the server is on.
