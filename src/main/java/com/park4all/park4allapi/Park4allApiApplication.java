/**
 * This is the main class of the Api server
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class Park4allApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Park4allApiApplication.class, args);
	}

}
