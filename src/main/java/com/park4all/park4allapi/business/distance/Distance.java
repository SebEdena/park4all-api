/**
 * This interface is implemented by the class Haversine
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.distance;

import com.park4all.park4allapi.entity.parking.bean.Geo;

public interface Distance {
    double getDistance(Geo start, Geo finish);
}
