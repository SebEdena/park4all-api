/**
 * This class implements Distance
 * This class is used to give distance between two coordinates
 * It will return the distance as the crow flies
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.distance;

import com.park4all.park4allapi.entity.parking.bean.Geo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Qualifier("haversine")
public class Haversine implements Distance {

    @Value("${earth.radius}")
    private double AVERAGE_RADIUS_OF_EARTH_M;

    /**
     * This function will return the distance in meter, between the two coordinates given
     * @param start
     * @param finish
     * @return
     */
    public double getDistance(Geo start, Geo finish) {
        double startLatitudeRadian = Math.toRadians(start.getLatitude());
        double startLongitudeRadian = Math.toRadians(start.getLongitude());
        double finishLatitudeRadian = Math.toRadians(finish.getLatitude());
        double finishLongitudeRadian = Math.toRadians(finish.getLongitude());

        double tmpLatSinSquare = (1 - Math.cos((finishLatitudeRadian - startLatitudeRadian))) / 2;
        double tmpLongSinSquare = (1 - Math.cos((finishLongitudeRadian - startLongitudeRadian))) / 2;

        return 2 * AVERAGE_RADIUS_OF_EARTH_M * Math.asin(Math.sqrt(tmpLatSinSquare + Math.cos(startLatitudeRadian) * Math.cos(finishLatitudeRadian) * tmpLongSinSquare));
    }

}

