/**
 * This interface is implemented by the class GraphContextImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.business.graph.context;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.score.criteria.Criterion;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.score.statistics.Statistic;
import org.jgrapht.Graph;

public interface GraphContext {
    Statistic getStatistic(Criterion key);

    void fillStatistics(Vehicle v, int radius) throws IllegalStateException;

    void setGraph(Graph<Park, Weight> graph);

    Graph<Park, Weight> getGraph();
}
