/**
 * This class implements GraphContextImpl
 * This class is used to have a context for the search of car parks for the user
 * This class contains also the subgraph for the search of car parks for the user
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.graph.context;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.score.criteria.Criterion;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import com.park4all.park4allapi.entity.score.statistics.Statistic;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;

import java.util.HashMap;
import java.util.Map;

import static com.park4all.park4allapi.entity.score.criteria.Criterion.*;

@Slf4j
@NoArgsConstructor
public class GraphContextImpl implements GraphContext {
    @Getter
    @Setter
    private Graph<Park, Weight> graph;

    private Map<Criterion, Statistic> statisticMap;

    @Override
    public Statistic getStatistic(Criterion key){
        return statisticMap.getOrDefault(key, null);
    }

    /**
     * This function will give all metrics from the graph
     * @param v
     * @param radius
     * @throws IllegalStateException
     */
    @Override
    public void fillStatistics(Vehicle v, int radius) throws IllegalStateException {
        if (graph == null) throw new IllegalStateException("The graph is null");

        statisticMap = new HashMap<>();
        statisticMap.put(VERTEX_PRICE, new Statistic());
        statisticMap.put(VERTEX_DISTANCE, new Statistic());
        statisticMap.put(VERTEX_SPECIFIC_PLACES, new Statistic());
        statisticMap.put(VERTEX_ALTERNATIVE_PLACES, new Statistic());
        statisticMap.put(HEURISTIC, new Statistic());
        statisticMap.put(EDGE_DISTANCE, new Statistic());

        Statistic statPrice = statisticMap.get(VERTEX_PRICE);
        Statistic statVertexDistance = statisticMap.get(VERTEX_DISTANCE);
        Statistic statPlaceSpecific = statisticMap.get(VERTEX_SPECIFIC_PLACES);
        Statistic statPlaceAlternative = statisticMap.get(VERTEX_ALTERNATIVE_PLACES);
        Statistic statHeuristic = statisticMap.get(HEURISTIC);
        Statistic statEdgeDistance = statisticMap.get(EDGE_DISTANCE);

        //Set min and max for distance
        statVertexDistance.setMin(0d);
        statVertexDistance.setMax((double) radius);

        statHeuristic.setMin(0d);
        statHeuristic.setMax((double) radius);

        for(Park carPark: graph.vertexSet()){
            if(carPark.getType().equals(Type.TECHNICAL)) continue;

            //Find min and max prices
            if (statPrice.getMin() == null || statPrice.getMin() > v.getPrice(carPark)) statPrice.setMin(v.getPrice(carPark));
            if (statPrice.getMax() == null || statPrice.getMax() < v.getPrice(carPark)) statPrice.setMax(v.getPrice(carPark));

            //Find min and max specific places
            if (statPlaceSpecific.getMin() == null || statPlaceSpecific.getMin() > v.getSpecificPlaces(carPark)) statPlaceSpecific.setMin((double) v.getSpecificPlaces(carPark));
            if (statPlaceSpecific.getMax() == null || statPlaceSpecific.getMax() < v.getSpecificPlaces(carPark)) statPlaceSpecific.setMax((double) v.getSpecificPlaces(carPark));

            //Find min and max alternative places
            if (statPlaceAlternative.getMin() == null || statPlaceAlternative.getMin() > v.getAlternativePlaces(carPark)) statPlaceAlternative.setMin((double) v.getAlternativePlaces(carPark));
            if (statPlaceAlternative.getMax() == null || statPlaceAlternative.getMax() < v.getAlternativePlaces(carPark)) statPlaceAlternative.setMax((double) v.getAlternativePlaces(carPark));
        }

        for(Weight w: graph.edgeSet()){

            //Find min and max edges
            if (statEdgeDistance.getMin() == null || statEdgeDistance.getMin() > w.getWeight()) statEdgeDistance.setMin(w.getWeight());
            if (statEdgeDistance.getMax() == null || statEdgeDistance.getMax() < w.getWeight()) statEdgeDistance.setMax(w.getWeight());
        }
    }

}
