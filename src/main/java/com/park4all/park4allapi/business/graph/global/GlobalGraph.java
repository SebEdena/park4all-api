/**
 * This interface is implemented by the class GlobalGraphImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.graph.global;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import org.jgrapht.Graph;

public interface GlobalGraph {

    Graph<Park, Weight> getGraph();

    GraphContext createSubGraph(Park start, Park finish, int radius, Vehicle vehicle);

}
