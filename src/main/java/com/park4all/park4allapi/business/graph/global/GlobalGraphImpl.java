/**
 * This class implements GlobalGraph
 * This class is used to have the graph of all car parks of Paris
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.graph.global;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.business.graph.context.GraphContextImpl;
import com.park4all.park4allapi.business.utils.csv.ParkingParser;
import com.park4all.park4allapi.business.utils.graph.GraphUtils;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.graph.AsSubgraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
public class GlobalGraphImpl implements GlobalGraph {

    @Value("${graph.links.perTechnicalPark}")
    private int linksPerTechnicalPark;

    @Value("${graph.links.perMergedPark}")
    private int linksPerMergedPark;

    @Value("${resource.parkingResource}")
    private String parkingsResource;

    @Value("${resource.linkResource}")
    private String linksResource;

    private final Distance distanceCalculator;

    private final ParkingParser parkingParser;

    private final GraphUtils graphUtils;

    @Getter
    private Graph<Park, Weight> graph;

    @Autowired
    public GlobalGraphImpl(Distance distanceCalculator, ParkingParser parkingParser, GraphUtils graphUtils) {
        this.distanceCalculator = distanceCalculator;
        this.parkingParser = parkingParser;
        this.graphUtils = graphUtils;
    }

    /**
     * This function will initialise the graph with all car parks and links between them
     * @throws IOException
     */
    @PostConstruct
    public void initialize() throws IOException {
        log.info("Starting initialise Graph");
        graph = graphUtils.initEmptyGraph();
        Map<String, Park> carparks = parkingParser.parseParkings(parkingsResource);
        List<Weight> weights = parkingParser.parseWeights(linksResource);
        graphUtils.initGraph(graph, carparks, weights);
        log.info("Number of Vertices : " + graph.vertexSet().size());
        log.info("Number of Edges : " + graph.edgeSet().size());
        log.info("Ending initialise Graph");
    }

    /**
     * This function will extract from the Global graph, a subgraph that the API server will work on it
     * to find car parks for the user
     * @param start is the first point from the navigation path
     * @param finish is the finish point
     * @param radius is the distance of research
     * @param vehicle is the type of vehicle
     * @return
     */
    @Override
    public GraphContext createSubGraph(Park start, Park finish, int radius, Vehicle vehicle){
        Set<Park> carparks = graph.vertexSet()
                                  .stream()
                                  .filter(carpark -> distanceCalculator.getDistance(carpark.getGeo(), finish.getGeo()) <= radius)
                                  .filter(vehicle.getFilter())
                                  .collect(Collectors.toSet());

        Graph<Park, Weight> sourceGraph = new AsSubgraph<>(graph, carparks);
        Graph<Park, Weight> resultGraph = graphUtils.initEmptyGraph();

        Graphs.addAllVertices(resultGraph, sourceGraph.vertexSet());
        Graphs.addAllEdges(resultGraph, sourceGraph, sourceGraph.edgeSet());

        int before = graphUtils.countSubGraph(resultGraph);

        if(before > 0) {
            mergeSubgraphs(resultGraph, linksPerMergedPark);
            insertTechnicalCarparks(start, finish, sourceGraph, resultGraph, linksPerTechnicalPark);
            int after = graphUtils.countSubGraph(resultGraph);
            log.info(String.format("Merged from %d subgraph(s) to %d subgraph(s).", before, after));
        } else {
            insertTechnicalCarparks(start, finish, sourceGraph, resultGraph, linksPerTechnicalPark);
            log.warn("No carparks found for the queried graph.");
        }

        GraphContext graphContext = new GraphContextImpl();
        graphContext.setGraph(resultGraph);
        graphContext.fillStatistics(vehicle, radius);
        return graphContext;
    }

    /**
     * This function is call in order to merge some subgraph to have a complete graph
     * @param resultGraph
     * @param linksPerCarpark
     */
    private void mergeSubgraphs(Graph<Park, Weight> resultGraph, int linksPerCarpark) {
        List<Set<Park>> subgraphs = graphUtils.extractSubGraph(resultGraph);

        while(subgraphs.size() > 1) {
            Set<Park> subgraphToConnect = subgraphs.get(0);
            Set<Park> biggestSubgraph = subgraphs.get(subgraphs.size() - 1);
            for(Park park : subgraphToConnect) {
                biggestSubgraph.stream()
                               .sorted((p1, p2) -> Comparator.comparingDouble(end ->
                                       distanceCalculator.getDistance(park.getGeo(), ((Park) end).getGeo()))
                                       .compare(p1, p2))
                               .limit(linksPerCarpark)
                               .forEach(carpark -> {
                                   Weight weight = new Weight(park.getId(), carpark.getId(), distanceCalculator.getDistance(park.getGeo(), carpark.getGeo()));
                                   if(resultGraph.addEdge(park, carpark, weight)) resultGraph.setEdgeWeight(weight, weight.getWeight());
                               });
            }
            subgraphs = graphUtils.extractSubGraph(resultGraph);
        }
    }

    /**
     * This function will insert the start and finish point in the subgraph.
     * They will have links between some car parks in order to do a shortest path on the subgraph
     * @param start
     * @param finish
     * @param sourceGraph
     * @param resultGraph
     * @param linksPerTechnicalCarpark
     */
    private void insertTechnicalCarparks(Park start, Park finish, Graph<Park, Weight> sourceGraph, Graph<Park, Weight> resultGraph, int linksPerTechnicalCarpark) {

        //Insert start & finish vertices
        resultGraph.addVertex(start);
        resultGraph.addVertex(finish);

        //Add start to result graph
        sourceGraph.vertexSet()
                .stream()
                .filter(carpark -> !carpark.equals(start) && !carpark.equals(finish))
                .sorted(Comparator.comparingDouble(carpark -> distanceCalculator.getDistance(start.getGeo(), carpark.getGeo())))
                .limit(linksPerTechnicalCarpark)
                .forEach(carpark -> {
                    Weight weight = new Weight(start.getId(), carpark.getId(), distanceCalculator.getDistance(start.getGeo(), carpark.getGeo()));
                    if(resultGraph.addEdge(start, carpark, weight))
                        resultGraph.setEdgeWeight(weight, weight.getWeight());
                });

        //Add finish to result graph
        sourceGraph.vertexSet()
                .stream()
                .filter(carpark -> !carpark.equals(start) && !carpark.equals(finish))
                .sorted(Comparator.comparingDouble(carpark -> distanceCalculator.getDistance(carpark.getGeo(), finish.getGeo())))
                .limit(linksPerTechnicalPark)
                .forEach(carpark -> {
                    Weight weight = new Weight(carpark.getId(), finish.getId(), distanceCalculator.getDistance(carpark.getGeo(), finish.getGeo()));
                    if(resultGraph.addEdge(carpark, finish, weight))
                        resultGraph.setEdgeWeight(weight, weight.getWeight());
                });
    }

}
