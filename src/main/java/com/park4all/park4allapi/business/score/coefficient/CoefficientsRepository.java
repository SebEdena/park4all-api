/**
 * This interface is implemented by the class CoefficientsRepositoryImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.score.coefficient;

import com.park4all.park4allapi.entity.score.criteria.Criterion;

public interface CoefficientsRepository {

    Double getCoefficient(Criterion c);

}
