/**
 * This class implements the interface CoefficientsRepository
 * This class will store the values of all coefficients use in our process to find car parks
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.score.coefficient;

import com.park4all.park4allapi.entity.score.criteria.Criterion;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CoefficientsRepositoryImpl implements CoefficientsRepository {

    @Value("${score.coef.price}")
    private Double vertexPrice;

    @Value("${score.coef.vertex.distance}")
    private Double vertexDistance;

    @Value("${score.coef.place.specific}")
    private Double vertexSpecificPlace;

    @Value("${score.coef.place.alternative}")
    private Double vertexAlternativePlace;

    @Value("${score.coef.heuristic}")
    private Double heuristic;

    @Value("${score.coef.edge.distance}")
    private Double edgeDistance;

    /**
     * This function will return the value of a coefficient from a Criterion
     * @param c
     * @return
     */
    @Override
    public Double getCoefficient(Criterion c) {
        switch (c) {
            case VERTEX_PRICE: return vertexPrice;
            case VERTEX_DISTANCE: return vertexDistance;
            case VERTEX_SPECIFIC_PLACES: return vertexSpecificPlace;
            case VERTEX_ALTERNATIVE_PLACES: return vertexAlternativePlace;
            case HEURISTIC: return heuristic;
            case EDGE_DISTANCE: return edgeDistance;
            default: return null;
        }
    }
}
