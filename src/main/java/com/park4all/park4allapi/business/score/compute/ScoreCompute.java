/**
 * This interface is implemented by the class ScoreComputeIml
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.score.compute;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.score.priority.Priority;

import java.util.Map;

public interface ScoreCompute {
    Double getCarparkScore(Park carpark, Park destination, GraphContext graphContext, Vehicle v, Distance d, Map<Priority, Integer> priorities);

    Double getWeightScore(Weight weight, GraphContext graphContext);

    Double getHeuristicScore(Park carpark, Park destination, GraphContext graphContext, Distance d);
}
