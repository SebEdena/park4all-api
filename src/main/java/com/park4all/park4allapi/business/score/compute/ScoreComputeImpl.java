/**
 * This class implements the interface CoefficientsRepository
 * This class will calculate the scores of all parameters with the coefficients
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.business.score.compute;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.score.coefficient.CoefficientsRepository;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import com.park4all.park4allapi.entity.score.priority.Priority;
import com.park4all.park4allapi.entity.score.statistics.Statistic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

import static com.park4all.park4allapi.entity.score.criteria.Criterion.*;

@Slf4j
@Component
public class ScoreComputeImpl implements ScoreCompute {

    private CoefficientsRepository coefficientsRepository;

    @Autowired
    public ScoreComputeImpl(CoefficientsRepository coefficientsRepository) {
        this.coefficientsRepository = coefficientsRepository;
    }

    /**
     * This function will give a score for a car park candidate. It depends of some parameters.
     * @param carpark
     * @param destination
     * @param graphContext
     * @param v
     * @param d
     * @param priorities
     * @return
     */
    @Override
    public Double getCarparkScore(Park carpark, Park destination, GraphContext graphContext, Vehicle v, Distance d, Map<Priority, Integer> priorities) {
        if(!carpark.getType().equals(Type.TECHNICAL)){
            Double resultPrice = carparkScorePrice(carpark, graphContext, v);
            Double resultDistance = carparkScoreDistance(carpark, destination, graphContext, d);
            Double resultSpecificPlaces = carparkScoreSpecificPlaces(carpark, graphContext, v);
            Double resultAlternativePlaces = carparkScoreAlternativePlaces(carpark, graphContext, v);

            Double userPrice = userCoefficient(priorities, Priority.PRICE);
            Double userDistance = userCoefficient(priorities, Priority.DISTANCE);

            return (userPrice * resultPrice) + (userDistance * resultDistance) + resultSpecificPlaces + resultAlternativePlaces;
        } else return 0d;
    }

    /**
     * This function will calculate the weight score
     * @param weight
     * @param graphContext
     * @return
     */
    @Override
    public Double getWeightScore(Weight weight, GraphContext graphContext) {
        Statistic statWeight = graphContext.getStatistic(EDGE_DISTANCE);
        Double coefWeight = coefficientsRepository.getCoefficient(EDGE_DISTANCE);

        return coefWeight * scaleValue(weight.getWeight(), statWeight);
    }

    /**
     * This function will calculate the heuristic score
     * @param carpark
     * @param destination
     * @param graphContext
     * @param d
     * @return
     */
    @Override
    public Double getHeuristicScore(Park carpark, Park destination, GraphContext graphContext, Distance d) {
        Statistic statHeuristic = graphContext.getStatistic(HEURISTIC);
        Double coefHeuristic = coefficientsRepository.getCoefficient(HEURISTIC);

        return coefHeuristic * scaleValue(d.getDistance(carpark.getGeo(), destination.getGeo()), statHeuristic);
    }

    /**
     * This function will scale value between 0 to 1
     * @param value
     * @param statistic
     * @return
     */
    private Double scaleValue(Double value, Statistic statistic) {
        if (statistic.getMax() - statistic.getMin() == 0) return 0d;
        else return (value - statistic.getMin())/(statistic.getMax() - statistic.getMin());
    }

    /**
     * This function will add in the calculation the user's preferences
     * @param prioritiesMap
     * @param priority
     * @return
     */
    private Double userCoefficient(Map<Priority, Integer> prioritiesMap, Priority priority){
        double valuePriority = (double) prioritiesMap.get(priority);
        return 1/(1+valuePriority);
    }

    /**
     * This function will return the score for a price
     * @param carpark
     * @param graphContext
     * @param v
     * @return
     */
    private Double carparkScorePrice (Park carpark, GraphContext graphContext, Vehicle v) {
        Statistic statPrice = graphContext.getStatistic(VERTEX_PRICE);
        Double coefPrice = coefficientsRepository.getCoefficient(VERTEX_PRICE);

        return coefPrice * scaleValue(v.getPrice(carpark), statPrice);
    }

    /**
     * This function will return the score for a distance
     * @param carpark
     * @param destination
     * @param graphContext
     * @param d
     * @return
     */
    private Double carparkScoreDistance (Park carpark, Park destination,  GraphContext graphContext, Distance d) {
        Statistic statDistance = graphContext.getStatistic(VERTEX_DISTANCE);
        Double coefDistance = coefficientsRepository.getCoefficient(VERTEX_DISTANCE);

        return coefDistance * scaleValue(d.getDistance(carpark.getGeo(), destination.getGeo()), statDistance);
    }

    /**
     * This function will return a score for the specific place
     * @param carpark
     * @param graphContext
     * @param v
     * @return
     */
    private Double carparkScoreSpecificPlaces (Park carpark, GraphContext graphContext, Vehicle v) {
        Statistic statSpecificPlaces = graphContext.getStatistic(VERTEX_SPECIFIC_PLACES);
        Double coefSpecificPlaces = coefficientsRepository.getCoefficient(VERTEX_SPECIFIC_PLACES);

        return coefSpecificPlaces * (1 - scaleValue((double) v.getSpecificPlaces(carpark), statSpecificPlaces));
    }

    /**
     * This function will return a score for the alternative place
     * @param carpark
     * @param graphContext
     * @param v
     * @return
     */
    private Double carparkScoreAlternativePlaces (Park carpark, GraphContext graphContext, Vehicle v) {
        Statistic statAlternativePlaces = graphContext.getStatistic(VERTEX_ALTERNATIVE_PLACES);
        Double coefAlternativePlaces = coefficientsRepository.getCoefficient(VERTEX_ALTERNATIVE_PLACES);

        return coefAlternativePlaces * (1 - scaleValue((double) v.getAlternativePlaces(carpark), statAlternativePlaces));
    }
}
