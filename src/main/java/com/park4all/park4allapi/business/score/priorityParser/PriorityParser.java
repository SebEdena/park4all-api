/**
 * This interface is implemented by the class PriorityParserImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.business.score.priorityParser;

import com.park4all.park4allapi.entity.score.priority.Priority;

import java.util.List;
import java.util.Map;

public interface PriorityParser {
    List<Priority> parsePriorities(String[] priorities) throws IllegalArgumentException;

    Map<Priority, Integer> prioritiesToMap(List<Priority> priorities);
}
