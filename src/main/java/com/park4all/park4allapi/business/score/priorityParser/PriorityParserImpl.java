/**
 * This class implements the class PriorityParser
 * The class will conserve Priorities given by the user in order
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.score.priorityParser;

import com.park4all.park4allapi.entity.score.priority.Priority;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class PriorityParserImpl implements PriorityParser {

    /**
     * This function will return a list of priorities from a list of String
     * @param priorities
     * @return
     * @throws IllegalArgumentException
     */
    @Override
    public List<Priority> parsePriorities(String[] priorities) throws IllegalArgumentException {
        List<Priority> result = new ArrayList<>();

        List<Priority> prioritiesStack = new ArrayList<>(Arrays.asList(Priority.values()));

        for (String priority : priorities) {
            Priority p = Priority.getPriority(priority);

            if (p == null)
                throw new IllegalArgumentException(String.format("The string %s cannot be parsed to a priority.", priority));
            if (result.contains(p))
                throw new IllegalArgumentException(String.format("Duplicate priority %s in array.", priority));

            prioritiesStack.remove(p);

            result.add(p);
        }

        if(prioritiesStack.size() > 0)
            throw new IllegalArgumentException("The following priorities are missing : " + Arrays.toString(prioritiesStack.toArray()));

        return result;
    }

    /**
     * This function will return a Map with a position for each Priority
     * @param priorities
     * @return
     */
    @Override
    public Map<Priority, Integer> prioritiesToMap(List<Priority> priorities) {
        Map<Priority, Integer> result = new HashMap<>();

        for (int i=0; i < priorities.size(); i++){
            result.put(priorities.get(i), i);
        }

        return result;
    }
}
