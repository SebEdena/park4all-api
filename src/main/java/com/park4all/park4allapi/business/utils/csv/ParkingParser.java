/**
 * This interface is implemented by the class PriorityParserImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.utils.csv;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ParkingParser {

    Map<String, Park> parseParkings(String resourcePath) throws IOException;

    List<Weight> parseWeights(String resourcePath) throws IOException;

}
