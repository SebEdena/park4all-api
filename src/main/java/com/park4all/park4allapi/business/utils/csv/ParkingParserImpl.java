/**
 * This class implements the class PriorityParser
 * The class will parse from csv data
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.utils.csv;

import com.opencsv.bean.CsvToBeanBuilder;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ParkingParserImpl implements ParkingParser {

    /**
     * This function will parse car parks data from csv files in to Park java object
     * @param resourcePath
     * @return
     * @throws IOException
     */
    @Override
    public Map<String, Park> parseParkings(String resourcePath) throws IOException {
        FileReader file = new FileReader(new ClassPathResource(resourcePath).getFile());
        List<Park> parks = new CsvToBeanBuilder<Park>(file).withSeparator(';').withType(Park.class).build().parse();

        Map<String, Park> parksMap = new HashMap<>();
        parks.forEach(park -> { if(park.getId() != null) parksMap.put(park.getId(), park); });

        return parksMap;
    }

    /**
     * This function will parse link od car parks data from csv files in to Weight java object
     * @param resourcePath
     * @return
     * @throws IOException
     */
    @Override
    public List<Weight> parseWeights(String resourcePath) throws IOException {
        FileReader file = new FileReader(new ClassPathResource(resourcePath).getFile());
        return new CsvToBeanBuilder<Weight>(file).withSeparator(';').withType(Weight.class).build().parse();
    }

}
