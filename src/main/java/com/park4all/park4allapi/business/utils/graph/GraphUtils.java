/**
 * This interface is implemented by the class GraphUtilsImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.utils.graph;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import org.jgrapht.Graph;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GraphUtils {

    void initGraph(Graph<Park, Weight> graph, Map<String, Park> carparks, List<Weight> weights);

    Graph<Park, Weight> initEmptyGraph();

    int countSubGraph(Graph<Park, Weight> graph);

    List<Set<Park>> extractSubGraph(Graph<Park, Weight> graph);
}
