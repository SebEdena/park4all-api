/**
 * This class implements GraphUtils
 * This class is used to do some actions on a graph
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.utils.graph;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.springframework.stereotype.Component;

import java.util.*;

@Slf4j
@Component
public class GraphUtilsImpl implements GraphUtils {

    /**
     * This graph will create the graph from a list of car parks and a list of links
     * @param graph
     * @param carparks
     * @param weights
     */
    @Override
    public void initGraph(Graph<Park, Weight> graph, Map<String, Park> carparks, List<Weight> weights) {
        for(Park carpark : carparks.values()) {
            graph.addVertex(carpark);
        }

        for(Weight weight : weights) {
            Park start = carparks.get(weight.getIdStart());
            Park finish = carparks.get(weight.getIdFinish());
            if(graph.addEdge(start, finish, weight)){
                graph.setEdgeWeight(weight, weight.getWeight());
            }
        }
    }

    /**
     * This function will create an empty graph that will be used after
     * @return
     */
    @Override
    public Graph<Park, Weight> initEmptyGraph() {
        return GraphTypeBuilder.
                <Park, Weight> undirected()
                .allowingMultipleEdges(false)
                .allowingSelfLoops(false)
                .edgeClass(Weight.class)
                .weighted(true)
                .buildGraph();
    }

    /**
     * This function will count the number of subgraph in a graph
     * @param graph
     * @return
     */
    @Override
    public int countSubGraph(Graph<Park, Weight> graph) {
        return extractSubGraph(graph).size();
    }

    /**
     * This function will retreive list of parks of each subgraph in the graph
     * @param graph
     * @return
     */
    @Override
    public List<Set<Park>> extractSubGraph(Graph<Park, Weight> graph) {
        ConnectivityInspector<Park, Weight> inspector = new ConnectivityInspector<>(graph);
        List<Set<Park>> subgraphs = new ArrayList<>(inspector.connectedSets());
        subgraphs.sort(Comparator.comparingInt(Set::size));
        return subgraphs;
    }

}
