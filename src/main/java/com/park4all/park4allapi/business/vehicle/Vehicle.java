/**
 * This interface is used by classes Car, Electric, Motorcycle, Prm
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.park4all.park4allapi.entity.business.json.VehicleSerializer;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.ParkMetadata;

import java.util.List;
import java.util.function.Predicate;

@JsonSerialize(using = VehicleSerializer.class)
public interface Vehicle {

    String getName();

    Predicate<Park> getFilter();

    double getPrice(Park carpark);

    int getSpecificPlaces(Park carpark);

    int getAlternativePlaces(Park carpark);

    List<ParkMetadata> fillCarparks(List<Park> carparks);

}
