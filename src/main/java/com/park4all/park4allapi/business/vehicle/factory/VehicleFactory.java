/**
 * This interface is implemented by the class VehicleFactoryImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.factory;

import com.park4all.park4allapi.business.vehicle.Vehicle;

public interface VehicleFactory {

    Vehicle getVehicle(String type);

}
