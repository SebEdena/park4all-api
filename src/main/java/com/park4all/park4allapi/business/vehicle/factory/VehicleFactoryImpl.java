/**
 * This class implements the class VehicleFactory
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.factory;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class VehicleFactoryImpl implements VehicleFactory {

    private final ApplicationContext context;

    @Autowired
    public VehicleFactoryImpl(ApplicationContext context) {
        this.context = context;
    }

    /**
     * This function will return a type of vehicle
     * @param type
     * @return
     */
    @Override
    public Vehicle getVehicle(String type) {
        if(context.containsBean(type) && context.isTypeMatch(type, Vehicle.class)) {
            return (Vehicle) context.getBean(type);
        } else {
            throw new IllegalArgumentException("The vehicle [" + type + "] does not exist.");
        }
    }
}
