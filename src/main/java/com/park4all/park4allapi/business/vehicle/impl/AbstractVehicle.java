/**
 * This is an abstract class for the types of vehicles
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.ParkMetadata;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public abstract class AbstractVehicle implements Vehicle {

    public abstract String getName();

    public abstract Predicate<Park> getFilter();

    public abstract double getPrice(Park carpark);

    public abstract int getSpecificPlaces(Park carpark);

    public abstract int getAlternativePlaces(Park carpark);

    public List<ParkMetadata> fillCarparks(List<Park> carparks) {
        List<ParkMetadata> result = new ArrayList<>();
        for(Park carpark : carparks) {
            ParkMetadata pm = new ParkMetadata();
            pm.setPark(carpark);
            pm.setPrice(this.getPrice(carpark));
            pm.setSpecificPlaces(this.getSpecificPlaces(carpark));
            pm.setAlternativePlaces(this.getAlternativePlaces(carpark));
            result.add(pm);
        }
        return result;
    }

    public String toString() {
        return this.getName();
    }
}
