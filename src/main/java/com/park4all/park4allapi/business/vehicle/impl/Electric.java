/**
 * This class defines properties of an electrical car
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import org.springframework.beans.factory.annotation.Value;

import java.util.function.Predicate;

public class Electric extends AbstractVehicle  {

    @Value("${vehicle.electric.rechargePrice}")
    private double rechargePrice;

    @Override
    public String getName() {
        return "electric";
    }

    /**
     * This function will return a method that will return Park that is compatible with eletrical car
     * A place is compatible if the place is a Payant Mixte / Payant Rotatif / Gratuit / Electric or a Private car park
     * @return
     */
    @Override
    public Predicate<Park> getFilter() {

        return (Park -> {
            boolean isCar = Park.getCar() != null && Park.getCar();
            boolean hasPrice = Park.getPrice1Hour() != null;
            boolean isPublic = Park.getType() != null && Park.getType().equals(Type.PUBLIC);
            boolean isCompatiblePublicSpot = Park.getPrioritySystem() != null &&
                    (Park.getPrioritySystem().equals(PrioritySystem.ELECTRIQUE)) ||
                    (Park.getPrioritySystem().equals(PrioritySystem.PAYANT_MIXTE) ||
                            Park.getPrioritySystem().equals(PrioritySystem.PAYANT_ROTATIF) ||
                            Park.getPrioritySystem().equals(PrioritySystem.GRATUIT));
            return  isCar && hasPrice && (!isPublic || isCompatiblePublicSpot);
        });
    }

    /**
     * This function will return the price of the car park
     * @param carpark
     * @return
     */
    @Override
    public double getPrice(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean isRecharge = carpark.getType() != null && carpark.getPrioritySystem().equals(PrioritySystem.ELECTRIQUE);
        if(isPublic && isRecharge){
            return rechargePrice;
        }

        else {
            return carpark.getPrice1Hour();
        }
    }

    /**
     * The function will return the number of specific place
     * It counts if it is a place Electrique or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getSpecificPlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean isCompatiblePublicSpot = carpark.getPrioritySystem() != null &&
                (carpark.getPrioritySystem().equals(PrioritySystem.ELECTRIQUE));

        if(isPublic){
            if(isCompatiblePublicSpot){
                return carpark.getTotalPark();
            }else{
                return 0;
            }
        }

        else{
            if (carpark.getElectricalCar()) {
                return carpark.getTotalPark() != null ? 1 : 0;
            }else{
                return 0;
            }
        }
    }

    /**
     * This function will return the number of alternative place
     * It counts if it is a place Payant Mixte / Payant Rotatif / Gratuit or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getAlternativePlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean specificElectric = carpark.getPrioritySystem() != null && carpark.getPrioritySystem().equals(PrioritySystem.ELECTRIQUE);
        boolean isCompatiblePublicSpot = carpark.getPrioritySystem() != null &&
                (carpark.getPrioritySystem().equals(PrioritySystem.PAYANT_MIXTE) ||
                        carpark.getPrioritySystem().equals(PrioritySystem.PAYANT_ROTATIF) ||
                        carpark.getPrioritySystem().equals(PrioritySystem.GRATUIT));

        if(isPublic){
            if(specificElectric || !isCompatiblePublicSpot){
                return 0;
            }
            else{
                return carpark.getTotalPark();
            }
        }

        else {
            int totalPark = (carpark.getTotalPark() != null && carpark.getTotalPark() > 0 )? carpark.getTotalPark() - 1 : 0;
            if (carpark.getElectricalCar()){
                return totalPark;
            }
            else{
                return carpark.getTotalPark();
            }
        }
    }
}
