/**
 * This class defines properties of a motorcycle
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;

import java.util.function.Predicate;

public class Motorcycle extends AbstractVehicle {

    @Override
    public String getName() {
        return "motorcycle";
    }

    /**
     * This function will return a method that will return Park that is compatible with eletrical car
     * A place is compatible if the place is a Payant Mixte / Payant Rotatif / Gratuit / Deux Roues or a Private car park
     * @return
     */
    @Override
    public Predicate<Park> getFilter() {
        return (Park -> {
            boolean isMotorCycle = (Park.getMotorcycle() != null && Park.getMotorcycle())
                    || (Park.getCar() != null && Park.getCar());
            boolean hasPrice = Park.getPrice1HourMotorcycle() != null || Park.getPrice1Hour() != null;
            boolean isPublic = Park.getType() != null && Park.getType().equals(Type.PUBLIC);
            boolean isCompatiblePublicSpot = Park.getPrioritySystem() != null &&
                    (Park.getPrioritySystem().equals(PrioritySystem.PAYANT_MIXTE) ||
                            Park.getPrioritySystem().equals(PrioritySystem.PAYANT_ROTATIF) ||
                            Park.getPrioritySystem().equals(PrioritySystem.GRATUIT) ||
                            Park.getPrioritySystem().equals(PrioritySystem.DEUX_ROUES));

            return isMotorCycle && hasPrice && (!isPublic || isCompatiblePublicSpot);
        });
    }

    /**
     * This function will return the price
     * @param carpark
     * @return
     */
    @Override
    public double getPrice(Park carpark) {
        return (carpark.getPrice1HourMotorcycle() == null)? carpark.getPrice1Hour() : carpark.getPrice1HourMotorcycle();
    }

    /**
     * The function will return the number of specific place
     * It counts if it is a place Deux roues or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getSpecificPlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean isCompatiblePublicSpot = carpark.getPrioritySystem() != null &&
                carpark.getPrioritySystem().equals(PrioritySystem.DEUX_ROUES);
        if(isPublic && !isCompatiblePublicSpot){
            return 0;
        }else{
            return carpark.getTotalPark() != null ? carpark.getTotalPark() : 0;
        }
    }

    /**
     * This function will return the number of alternative place
     * It counts if it is a place Payant Mixte / Payant Rotatif / Gratuit or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getAlternativePlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean isCompatiblePublicSpot = carpark.getPrioritySystem() != null &&
                (carpark.getPrioritySystem().equals(PrioritySystem.PAYANT_MIXTE) ||
                        carpark.getPrioritySystem().equals(PrioritySystem.PAYANT_ROTATIF) ||
                        carpark.getPrioritySystem().equals(PrioritySystem.GRATUIT));
        if(isPublic && !isCompatiblePublicSpot){
            return 0;
        }else{
            return carpark.getTotalPark() != null ? carpark.getTotalPark() : 0;
        }
    }
}
