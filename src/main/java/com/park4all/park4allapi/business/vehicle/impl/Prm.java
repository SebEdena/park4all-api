/**
 * This class defines properties of a car
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;

import java.util.function.Predicate;

public class Prm extends AbstractVehicle  {

    @Override
    public String getName() {
        return "prm";
    }

    /**
     * This function will return a method that will return Park that is compatible with Car
     * A place is compatible if the place is a Payant Mixte / Payant Rotatif / Gratuit / PMR or a Private car park
     * @return
     */
    @Override
    public Predicate<Park> getFilter(){
        return (Park -> {
            boolean isCar = Park.getCar() != null && Park.getCar();
            boolean isPublic = Park.getType() != null && Park.getType().equals(Type.PUBLIC);
            boolean isCompatiblePublicSpot = Park.getPrioritySystem() != null &&
                    (Park.getPrioritySystem().equals(PrioritySystem.PAYANT_MIXTE) ||
                            Park.getPrioritySystem().equals(PrioritySystem.PAYANT_ROTATIF) ||
                            Park.getPrioritySystem().equals(PrioritySystem.GRATUIT) ||
                            Park.getPrioritySystem().equals((PrioritySystem.PMR)));
            boolean hasSurfaceLift = Park.getSurfaceLift() != null && Park.getSurfaceLift();

            return isCar && ((isPublic && isCompatiblePublicSpot) || (!isPublic && hasSurfaceLift)) ;
        });
    }

    /**
     * This function will return the price
     * If the place is public then the price is 0
     * @param carpark
     * @return
     */
    @Override
    public double getPrice(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        if(isPublic){
            return 0;
        }
        else{
            return carpark.getPrice1Hour();
        }
    }

    /**
     * The function will return the number of specific place
     * It counts if it is a place PMR or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getSpecificPlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean specificPmr = carpark.getPrioritySystem() != null && carpark.getPrioritySystem().equals(PrioritySystem.PMR);
        if(isPublic){
            if(specificPmr){
                return carpark.getTotalPark();
            }else{
                return 0;
            }
        }else{
            return carpark.getParkPMR() == null ? 0 : carpark.getParkPMR();
        }
    }

    /**
     * This function will return the number of alternative place
     * It counts if it is a place Payant Mixte / Payant Rotatif / Gratuit or a Private car park
     * @param carpark
     * @return
     */
    @Override
    public int getAlternativePlaces(Park carpark) {
        boolean isPublic = carpark.getType() != null && carpark.getType().equals(Type.PUBLIC);
        boolean specificPmr = carpark.getPrioritySystem() != null && carpark.getPrioritySystem().equals(PrioritySystem.PMR);
        if(isPublic){
            if(specificPmr){
                return 0;
            }else{
                return carpark.getTotalPark();
            }
        }else{
            return carpark.getTotalPark() == null ? 0 : carpark.getTotalPark();
        }
    }
}
