/**
 * This class prepares some beans in order to use it
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.config.bean;

import com.park4all.park4allapi.business.vehicle.*;
import com.park4all.park4allapi.business.vehicle.impl.Car;
import com.park4all.park4allapi.business.vehicle.impl.Electric;
import com.park4all.park4allapi.business.vehicle.impl.Motorcycle;
import com.park4all.park4allapi.business.vehicle.impl.Prm;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class VehicleBeans {

    @Bean(name="car")
    @Scope(value = "prototype")
    Vehicle getCar() {
        return new Car();
    }

    @Bean(name="motorcycle")
    @Scope(value = "prototype")
    Vehicle getMotorcycle() {
        return new Motorcycle();
    }

    @Bean(name="electric")
    @Scope(value = "prototype")
    Vehicle getElectric() {
        return new Electric();
    }

    @Bean(name="prm")
    @Scope(value = "prototype")
    Vehicle getPrm() {
        return new Prm();
    }
}
