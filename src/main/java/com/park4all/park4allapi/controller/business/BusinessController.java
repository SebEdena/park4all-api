/**
 * This class works as a servlet and will process if a request is receive
 * Business Controller will answer with a path from a to b with car parks
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.controller.business;

import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.business.apiObject.ApiObjectService;
import com.park4all.park4allapi.service.business.ping.PingService;
import com.park4all.park4allapi.service.business.shortpath.ShortPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/process")
public class BusinessController {

    private PingService pingService;

    private ApiObjectService apiObjectService;

    private ShortPathService shortPath;

    @Autowired
    public BusinessController(PingService pingService, ApiObjectService apiObjectService, ShortPathService shortPath) {
        this.pingService = pingService;
        this.apiObjectService = apiObjectService;
        this.shortPath = shortPath;
    }

    /**
     * This function will give the path and car parks
     * @param start
     * @param finish
     * @param vehicle
     * @param radius
     * @param priorities
     * @param request
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping(value = "/path", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDefaultAnswer(
            @RequestParam("start") String start,
            @RequestParam("finish") String finish,
            @RequestParam("vehicle") String vehicle,
            @RequestParam("radius") int radius,
            @RequestParam("priorities") String priorities,
            HttpServletRequest request)
            throws ApiRunTimeException
    {
        ApiParam apiParam = apiObjectService.parseRequestParam(start, finish, vehicle, radius, priorities);
        int code = pingService.sendPingRequest(request, EnumRequestMethod.GET,"/process/ping");
        ApiResponse apiResponse = apiObjectService.initApiResponse(apiParam, code);

        shortPath.computePath(apiResponse);

        return new ResponseEntity<>(apiObjectService.parseApiResponseToJson(apiResponse), new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * This function will verify if the server is ok or not
     * It will return an integer
     * @return
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> ping(){
        return new ResponseEntity<>(HttpStatus.OK.value(), new HttpHeaders(), HttpStatus.OK);
    }

}
