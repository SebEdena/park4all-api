/**
 * This class works as a servlet and will process if a request is receive
 * MapboxController class will answer with a path from a to b with car parks
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.controller.business;

import com.park4all.park4allapi.entity.business.api.mapbox.MapboxParam;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.business.mapbox.object.MapboxObjectService;
import com.park4all.park4allapi.service.business.mapbox.service.MapboxService;
import com.park4all.park4allapi.service.business.ping.PingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/mapbox")
public class MapboxController {

    private MapboxService mapboxService;

    private MapboxObjectService mapboxObjectService;

    private PingService pingService;

    @Autowired
    public MapboxController(MapboxService mapboxService, MapboxObjectService mapboxObjectService, PingService pingService) {
        this.mapboxService = mapboxService;
        this.pingService = pingService;
        this.mapboxObjectService = mapboxObjectService;
    }

    /**
     * This function will give the path
     * @param start
     * @param finish
     * @param request
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping(value = "/path", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDefaultAnswer(
            @RequestParam("start") String start,
            @RequestParam("finish") String finish,
            HttpServletRequest request)
            throws ApiRunTimeException
    {
        MapboxParam mapboxParam = mapboxObjectService.parseRequestParam(start, finish);
        int code = pingService.sendPingRequest(request, EnumRequestMethod.GET,"/process/ping");
        MapboxResponse mapboxResponse = mapboxObjectService.initMapboxResponse(mapboxParam, code);

        mapboxService.getPath(mapboxResponse);

        return new ResponseEntity<>(mapboxObjectService.parseMapboxResponseToJson(mapboxResponse), new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * This function will verify if the server is ok or not
     * It will return an integer
     * @return
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> ping(){
        return new ResponseEntity<>(HttpStatus.OK.value(), new HttpHeaders(), HttpStatus.OK);
    }

}
