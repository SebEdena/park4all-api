/**
 * This class will handle Exception that the application catches and wil return a precise message
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.controller.business;

import com.park4all.park4allapi.entity.business.api.error.ApiError;
import com.park4all.park4allapi.entity.exception.business.*;
import com.park4all.park4allapi.entity.exception.utils.MyException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * This function will return information from a specific exception
     * @param ex
     * @return
     */
    @ExceptionHandler(value = { MyException.class, ParametersNotValidException.class})
    protected ResponseEntity<ApiError> handleMyException(ApiRunTimeException ex) {
        log.error("Exception thrown : "+ ex.getClass().getSimpleName());
        log.error("Exception message : "+ ex.getStatus() +" - "+ ex.getMessage());
        log.error("Exception causes : "+ ex.getApiError().getErrors());
        return new ResponseEntity<>(ex.getApiError(), new HttpHeaders(), ex.getStatus());
    }

    /**
     * This function will return information from a Service not available exception
     * @param ex
     * @return
     */
    @ExceptionHandler(value = { ServiceNotAvailableException.class})
    protected ResponseEntity<Object> handleServiceNotAvailableException(ApiRunTimeException ex) {
        log.error("Ping return status different from 200");
        log.error("Exception thrown : "+ ex.getClass().getSimpleName());
        log.error("Exception message : "+ ex.getApiError().getStatus() +" - "+ ex.getApiError().getMessage());
        return new ResponseEntity<>(ex.getApiError(), new HttpHeaders(), ex.getApiError().getStatus());
    }

    /**
     * This function will return information from a Failed to connect exception
     * @param ex
     * @return
     */
    @ExceptionHandler(value = { FailedToConnectException.class})
    protected ResponseEntity<Object> handleFailedToConnectException(ApiRunTimeException ex) {
        log.error("Exception thrown : "+ ex.getClass().getSimpleName());
        log.error("Exception message : "+ ex.getApiError().getStatus() +" - "+ ex.getApiError().getMessage());
        return new ResponseEntity<>(ex.getApiError(), new HttpHeaders(), ex.getApiError().getStatus());
    }

    /**
     * This function will return information from a Failed to connect Mapbox exception
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {FailedToConnectMapboxException.class})
    protected ResponseEntity<Object> handleFailedToConnectMapboxException(ApiRunTimeException ex) {
        log.error("Exception thrown : "+ ex.getClass().getSimpleName());
        log.error("Exception message : "+ ex.getApiError().getStatus() +" - "+ ex.getApiError().getMessage());
        return new ResponseEntity<>(ex.getApiError(), new HttpHeaders(), ex.getApiError().getStatus());
    }

    /**
     * This function will return information from a Failed to parse exception
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {FailedToParseException.class})
    protected ResponseEntity<Object> handleFailedToParseException(ApiRunTimeException ex) {
        log.error("Exception thrown : "+ ex.getClass().getSimpleName());
        log.error("Exception message : "+ ex.getApiError().getStatus() +" - "+ ex.getApiError().getMessage());
        log.error("Exception cause : "+ ex.getApiError().getErrors().toString());
        return new ResponseEntity<>(ex.getApiError(), new HttpHeaders(), ex.getApiError().getStatus());
    }
}
