/**
 * This class works as a servlet and will process if a request is receive
 * This was a test Class used during the project
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.controller.utils;

import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.business.apiObject.ApiObjectService;
import com.park4all.park4allapi.service.utils.DefaultJSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class DefaultController {

    private DefaultJSON defaultJSON;

    private ApiObjectService apiObjectService;

    @Autowired
    public DefaultController(DefaultJSON defaultJSON, ApiObjectService apiObjectService) {
        this.defaultJSON = defaultJSON;
        this.apiObjectService = apiObjectService;
    }

    /**
     * This function will return a default answer
     * @param start
     * @param finish
     * @param vehicle
     * @param radius
     * @param priorities
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping(value = "/path", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getDefaultAnswer(
            @RequestParam("start") String start,
            @RequestParam("finish") String finish,
            @RequestParam("vehicle") String vehicle,
            @RequestParam("radius") int radius,
            @RequestParam("priorities") String priorities)
            throws ApiRunTimeException
    {
        ApiParam apiParam = apiObjectService.parseRequestParam(start, finish, vehicle, radius, priorities);
        ApiResponse apiResponse = apiObjectService.initApiResponse(apiParam, 200);
        defaultJSON.getAnswer(apiResponse, "/default.json");

        return new ResponseEntity<>(apiObjectService.parseApiResponseToJson(apiResponse), new HttpHeaders(), HttpStatus.OK);
    }
}
