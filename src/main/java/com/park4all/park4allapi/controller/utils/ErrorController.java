/**
 * This class works as a servlet and will process if a request is receive
 * This was a test Class for exception used during the project
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.controller.utils;

import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;
import com.park4all.park4allapi.entity.exception.utils.MyException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test/exceptions")
public class ErrorController {

    /**
     * This function will return a simple exception
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping("/simple")
    public String getMyExceptionSimple () throws ApiRunTimeException {
        throw new MyException("Error thrown due to unknown reason");
    }

    /**
     * This function will return an exception 500
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping("/error500Simple")
    public String getError500Simple () throws ApiRunTimeException {
        throw new ParametersNotValidException("Missing parameter 3 : coucou");
    }

    /**
     * This function will return an exception 500
     * @return
     * @throws ApiRunTimeException
     */
    @RequestMapping("/error500Multi")
    public String getError500Multi () throws ApiRunTimeException {
        throw new ParametersNotValidException(
                "Type mismatch for parameter 1 : helloFrom",
                "Missing parameter 3 : theOther",
                "Missing parameter 4 : side");
    }
}
