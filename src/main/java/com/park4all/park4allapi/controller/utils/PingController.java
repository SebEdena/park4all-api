/**
 * This class works as a servlet and will process if a request is receive
 * This was a test Class for ping used during the project
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.controller.utils;

import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.utils.PingServiceTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/test")
public class PingController {

    private PingServiceTest pingServiceTest;

    @Autowired
    public PingController(PingServiceTest pingServiceTest) {
        this.pingServiceTest = pingServiceTest;
    }

    /**
     * This function will return a return code from a get method
     * @param request
     * @return
     * @throws ApiRunTimeException
     */
    @GetMapping("/ping")
    public int getPingByGET(HttpServletRequest request) throws ApiRunTimeException {
        return pingServiceTest.sendPingRequest(request, EnumRequestMethod.GET);
    }

    /**
     * This function will test the ping from a get method
     */
    @GetMapping("/getPing")
    public void getPing(){
        pingServiceTest.ping();
    }

    /**
     * This function will return a return code from a post method
     * @param request
     * @return
     * @throws ApiRunTimeException
     */
    @PostMapping("/ping")
    public int getPingByPost(HttpServletRequest request) throws ApiRunTimeException {
        return pingServiceTest.sendPingRequest(request, EnumRequestMethod.POST);
    }

    /**
     * This function will test the ping from a post method
     */
    @PostMapping("/getPing")
    public void postPing(){
        pingServiceTest.ping();
    }

}
