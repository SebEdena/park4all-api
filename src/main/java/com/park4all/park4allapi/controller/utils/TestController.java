/**
 * This class works as a servlet and will process if a request is receive
 * This was a test Class used during the project
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.controller.utils;

import com.park4all.park4allapi.service.utils.TestService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/test")
public class TestController {

    private TestService testService;

    @Autowired
    public TestController(TestService testService) {
        this.testService = testService;
    }

    /**
     * this function will return a json
     * @param param
     * @return
     */
    @RequestMapping("/json")
    public String getJSON(@RequestBody String param){
        JSONObject json = new JSONObject(param);
        return testService.readJSON(json);
    }
}
