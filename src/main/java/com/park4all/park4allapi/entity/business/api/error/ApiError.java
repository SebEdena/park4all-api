/**
 * This class has details of an API Error
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.business.api.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class ApiError {

    private int code;
    private HttpStatus status;
    private String message;

    private List<String> errors;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    public ApiError() {
        timestamp = LocalDateTime.now();
    }

    public ApiError(HttpStatus status, String message) {
        this();
        this.status = status;
        this.message = message;
        this.code = status.value();
    }

    @Override
    public String toString(){
        JSONObject apiError = new JSONObject();
        apiError.put("statut",status);
        apiError.put("code",code);
        apiError.put("timestamp",timestamp);
        apiError.put("message",message);
        apiError.put("errors",errors);
        return apiError.toString();
    }
}
