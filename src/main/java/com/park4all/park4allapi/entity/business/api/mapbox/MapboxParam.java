/**
 * This class is for mapbox parameters
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.business.api.mapbox;

import com.park4all.park4allapi.entity.parking.bean.Geo;
import lombok.Data;

@Data
public class MapboxParam {

    private Geo start;
    private Geo finish;

}
