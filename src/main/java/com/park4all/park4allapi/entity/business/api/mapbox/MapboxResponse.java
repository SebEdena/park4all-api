/**
 * This class is for mapbox Response
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.business.api.mapbox;

import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Park;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class MapboxResponse {

    private String status;
    private Integer code;
    private MapboxParam param;
    private Answer result;
    private List<Park> carparks;

    public void setCode(int code) {
        this.status = HttpStatus.resolve(code).getReasonPhrase();
        this.code = code;
    }
}
