/**
 * This class is for API parameters
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.business.api.process;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.score.priority.Priority;
import lombok.Data;

import java.util.List;

@Data
public class ApiParam {

    private Geo start;
    private Geo finish;
    private Vehicle vehicle;
    private Integer radius;
    private List<Priority> priorities;

}
