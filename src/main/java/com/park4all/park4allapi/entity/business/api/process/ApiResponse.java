/**
 * This class is for API Response
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.business.api.process;

import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.ParkMetadata;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
public class ApiResponse {

    private String status;
    private Integer code;
    private ApiParam param;
    private Answer result;
    private List<ParkMetadata> carparks;

    public void setCode(int code) {
        this.status = HttpStatus.resolve(code).getReasonPhrase();
        this.code = code;
    }
}
