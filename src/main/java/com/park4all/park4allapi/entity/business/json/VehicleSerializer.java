/**
 * This class is to serialise into json vehicle
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.business.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.park4all.park4allapi.business.vehicle.Vehicle;

import java.io.IOException;

public class VehicleSerializer extends StdSerializer<Vehicle> {

    public VehicleSerializer() {
        super(Vehicle.class);
    }

    @Override
    public void serialize(Vehicle vehicle, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(vehicle.getName());
    }
}
