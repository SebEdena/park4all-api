package com.park4all.park4allapi.entity.business.net;

import lombok.Getter;

@Getter
public enum EnumRequestMethod {
    GET,
    POST
}
