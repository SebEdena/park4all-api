/**
 * This class is for parameters of the API server
 */

package com.park4all.park4allapi.entity.business.net;

import lombok.Data;

@Data
public class ServeurURL {
    private String scheme;
    private String host;
    private int port;
    private String service;

    public ServeurURL(String scheme, String host, int port, String service) {
        this.scheme = scheme;
        this.host = host;
        this.port = port;
        this.service = service;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        if(host.equals("0:0:0:0:0:0:0:1"))
            sb.append(scheme).append("://").append("[").append(host).append("]").append(":").append(port).append(service);
         else sb.append(scheme).append("://").append(host).append(":").append(port).append(service);
        return sb.toString();
    }
}
