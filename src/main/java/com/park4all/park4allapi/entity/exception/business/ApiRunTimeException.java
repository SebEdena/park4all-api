/**
 * This abstract class is for any kind of error
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.exception.business;

import com.park4all.park4allapi.entity.business.api.error.ApiError;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

public abstract class ApiRunTimeException extends Exception {

    private ApiError apiError;

    public ApiRunTimeException(String message, HttpStatus status){
        apiError = new ApiError(status,message);
    }

    public void setCauses(String... errors){
        apiError.setErrors(Arrays.asList(errors));
    }

    public ApiError getApiError(){
        return apiError;
    }

    public HttpStatus getStatus(){
        return apiError.getStatus();
    }

    public String getMessage(){
        return apiError.getMessage();
    }


}
