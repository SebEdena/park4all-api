/**
 * This class is for the Failed to connect
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.business;

import org.springframework.http.HttpStatus;

public class FailedToConnectException extends ApiRunTimeException {
    private final static String ERROR_MESSAGE = "Could not connect to the server";
    private final static HttpStatus ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    private FailedToConnectException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public FailedToConnectException(String... errors){
        this();
        setCauses(errors);
    }
}
