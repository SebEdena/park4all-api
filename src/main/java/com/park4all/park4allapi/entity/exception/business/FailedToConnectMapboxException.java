/**
 * This class is for the Failed to connect to Mapbox
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.business;

import org.springframework.http.HttpStatus;

public class FailedToConnectMapboxException extends ApiRunTimeException {
    private final static String ERROR_MESSAGE = "Could not connect to Mapbox Server";
    private final static HttpStatus ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    private FailedToConnectMapboxException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public FailedToConnectMapboxException(String... errors){
        this();
        setCauses(errors);
    }
}
