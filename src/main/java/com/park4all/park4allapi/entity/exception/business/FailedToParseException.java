/**
 * This class is for the Failed to parse
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.business;

import org.springframework.http.HttpStatus;

public class FailedToParseException extends ApiRunTimeException {
    private final static String ERROR_MESSAGE = "Could not parse the data";
    private final static HttpStatus ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    private FailedToParseException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public FailedToParseException(String... errors){
        this();
        setCauses(errors);
    }
}
