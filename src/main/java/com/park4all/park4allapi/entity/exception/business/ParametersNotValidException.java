/**
 * This class is for the invalid parameters
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.business;

import org.springframework.http.HttpStatus;

public class ParametersNotValidException extends ApiRunTimeException {

    private final static String ERROR_MESSAGE = "Parameters given are not valid";
    private final static HttpStatus ERROR_STATUS = HttpStatus.BAD_REQUEST;

    private ParametersNotValidException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public ParametersNotValidException(String... errors){
        this();
        setCauses(errors);
    }
}
