/**
 * This class is for the unavailable to connect
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.business;

import org.springframework.http.HttpStatus;

public class ServiceNotAvailableException extends ApiRunTimeException {
    private final static String ERROR_MESSAGE = "Service not available";
    private final static HttpStatus ERROR_STATUS = HttpStatus.SERVICE_UNAVAILABLE;

    private ServiceNotAvailableException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public ServiceNotAvailableException(String... errors){
        this();
        setCauses(errors);
    }
}
