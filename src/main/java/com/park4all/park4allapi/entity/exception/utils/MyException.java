/**
 * This class is for a specific exception
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.exception.utils;

import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import org.springframework.http.HttpStatus;

public class MyException extends ApiRunTimeException {

    private final static String ERROR_MESSAGE = "Exception due to MyException";
    private final static HttpStatus ERROR_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

    private MyException() {
        super(ERROR_MESSAGE,ERROR_STATUS);
    }

    public MyException(String... errors){
        this();
        setCauses(errors);
    }
}
