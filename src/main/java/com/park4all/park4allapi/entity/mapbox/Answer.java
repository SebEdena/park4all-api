/**
 * The class Answer is to translate the result from the Mapbox service
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Answer {
    private String code;
    private String uuid;
    private List<Route> routes;
    private List<Waypoint> waypoints;
}
