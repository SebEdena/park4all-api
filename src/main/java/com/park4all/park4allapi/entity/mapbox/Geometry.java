/**
 * The class Geometry is to translate the result from the Mapbox service
 * This class contains all coordinates to display on the UI
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Geometry {
    private List<Double[]> coordinates;
    private String type;
}
