/**
 * The class Intersection is to translate the result from the Mapbox service
 * Intersection gives information to the UI to display correctly the navigation
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties (ignoreUnknown = true)
public class Intersection {
    private String out;
    private String in;
    private Boolean[] entry;
    private String[] bearings;
    private Double[] location;
}
