/**
 * The class Leg is to translate the result from the Mapbox service
 * Leg can give some Steps fron two intermediates points
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Leg {
    private String summary;
    private Double weight;
    private Double duration;
    private List<Step> steps;
    private Double distance;
}
