/**
 * The class Maneuver is to translate the result from the Mapbox service
 * Maneuver gives details to how to drive
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Maneuver {
    private String bearing_after;
    private String bearing_before;
    private Double[] location;
    private String modifier;
    private String type;
    private String instruction;
}
