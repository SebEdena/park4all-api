/**
 * The class Route is to translate the result from the Mapbox service
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Route {
    private Geometry geometry;
    private List<Leg> legs;
    private String weight_name;
    private Double weight;
    private Double duration;
    private Double distance;
}
