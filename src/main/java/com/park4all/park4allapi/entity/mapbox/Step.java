/**
 * The class Step is to translate the result from the Mapbox service
 * Step can give all instructions to a driver to drive
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Step {
    private List<Intersection> intersections;
    private String driving_side;
    private String geometry;
    private String mode;
    private Maneuver maneuver;
    private Double weight;
    private Double duration;
    private String name;
    private Double distance;
}
