/**
 * The class Waypoint is to translate the result from the Mapbox service
 * This class defines some intermediaite point
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.entity.mapbox;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Waypoint {
    private Double distance;
    private String name;
    private Double[] location;
}
