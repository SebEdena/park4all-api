/**
 * This class is to represent coordinates of a point
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.bean;

import com.opencsv.bean.CsvCustomBindByName;
import com.park4all.park4allapi.entity.parking.converter.StringToDoubleConverter;
import lombok.Data;

@Data
public class Geo {

    @CsvCustomBindByName(column="longitudeGeo", converter = StringToDoubleConverter.class)
    private Double longitude;

    @CsvCustomBindByName(column="latitudeGeo", converter = StringToDoubleConverter.class)
    private Double latitude;

    @Override
    public int hashCode() {
        return this.getLongitude().hashCode()^this.getLatitude().hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Geo)) return false;
        Geo otherGeo = (Geo) other;
        return this.getLongitude().equals(otherGeo.getLongitude())
                && this.getLatitude().equals(otherGeo.getLatitude());
    }

}
