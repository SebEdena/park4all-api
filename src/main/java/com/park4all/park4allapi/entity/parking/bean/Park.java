/**
 * This class is to represent information of a car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.bean;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.CsvRecurse;
import com.park4all.park4allapi.entity.parking.converter.*;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import lombok.Data;

@Data
public class Park {

    @CsvBindByName
    private String id;

    @CsvBindByName
    private String parkName;

    @CsvBindByName
    private String address;

    @CsvCustomBindByName(converter = StringToIntegerConverter.class)
    private Integer district;

    @CsvCustomBindByName(converter = ParkTypeConverter.class)
    private Type type;

    @CsvCustomBindByName(converter = StringToIntegerConverter.class)
    private Integer maxHeight;

    @CsvCustomBindByName(converter = StringNullConverter.class)
    private String schedule;

    @CsvCustomBindByName(converter = StringToBooleanConverter.class)
    private Boolean surfaceLift;

    @CsvCustomBindByName(converter = StringToBooleanConverter.class)
    private Boolean car;

    @CsvCustomBindByName(converter = StringToBooleanConverter.class)
    private Boolean motorcycle;

    @CsvCustomBindByName(converter = StringToBooleanConverter.class)
    private Boolean electricalCar;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double subscriptionMonth;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double price1Hour;

    @CsvCustomBindByName(converter = StringToIntegerConverter.class)
    private Integer parkPMR;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double subscriptionPMRMonth;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double subscriptionElectricalCarMonth;

    @CsvCustomBindByName(converter = StringToIntegerConverter.class)
    private Integer parkMotorcycle;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double price1HourMotorcycle;

    @CsvCustomBindByName(converter = StringToDoubleConverter.class)
    private Double subscriptionMotorcycleMonth;

    @CsvRecurse
    private Geo geo;

    @CsvCustomBindByName(converter = StringNullConverter.class)
    private String particularSystem;

    @CsvCustomBindByName(converter = PrioritySystemConverter.class)
    private PrioritySystem prioritySystem;

    @CsvCustomBindByName(converter = StringToIntegerConverter.class)
    private Integer totalPark;

    @Override
    public int hashCode() {
        return this.getId().hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Park)) return false;
        Park otherPark = (Park) other;
        return this.getId().equals(otherPark.getId());
    }
}
