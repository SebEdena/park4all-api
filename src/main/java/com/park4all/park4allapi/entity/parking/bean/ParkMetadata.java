/**
 * This class is to represent information for the UI of a car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.bean;

import lombok.Data;

@Data
public class ParkMetadata {

    private Park park;
    private Double price;
    private Integer specificPlaces;
    private Integer alternativePlaces;

}
