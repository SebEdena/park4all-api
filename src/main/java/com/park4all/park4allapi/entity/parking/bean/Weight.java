/**
 * This class is to represent an edge between car parks in the graph
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.bean;

import com.opencsv.bean.CsvCustomBindByName;
import com.park4all.park4allapi.entity.parking.converter.StringNullConverter;
import com.park4all.park4allapi.entity.parking.converter.StringToDoubleConverter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Weight {

    @CsvCustomBindByName(column = "id", converter = StringNullConverter.class)
    private String idStart;

    @CsvCustomBindByName(column = "joinId", converter = StringNullConverter.class)
    private String idFinish;

    @CsvCustomBindByName(column = "haversine", converter = StringToDoubleConverter.class)
    private Double weight;

    public boolean isCorrectWeight() {
        return idStart != null && idFinish != null && weight != null && weight >= 0d;
    }

    @Override
    public int hashCode() {
        return (this.getIdStart() + this.getIdFinish()).hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Weight)) return false;
        Weight otherWeight = (Weight) other;
        return otherWeight.getIdStart().equals(this.getIdStart())
                && otherWeight.getIdFinish().equals(this.getIdStart());
    }
}
