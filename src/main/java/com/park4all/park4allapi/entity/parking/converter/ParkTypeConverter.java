/**
 * This class will return a type of car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.converter;

import com.opencsv.bean.AbstractBeanField;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;

import static com.park4all.park4allapi.entity.parking.fieldEnum.Type.*;

public class ParkTypeConverter<T, I> extends AbstractBeanField<T, I> {

    @Override
    protected Type convert(String s) {
        switch (s) {
            case "public": return PUBLIC;
            case "technical": return TECHNICAL;
            case "INDIGO": return INDIGO;
            case "SAEMES": return SAEMES;
            case "SAGS": return SAGS;
            case "Q PARK": return Q_PARK;
            case "SOCIETE SEIH_ACCOR_ALLSEASONS": return SEIH_ACCOR_ALLSEASONS;
            case "INTERPARKING": return INTERPARKING;
            case "REALPARK S.A.": return REALPARK;
            default: return null;
        }
    }
}
