/**
 * This class will return a type of priority system
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.converter;

import com.opencsv.bean.AbstractBeanField;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;

import static com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem.*;

public class PrioritySystemConverter<T, I> extends AbstractBeanField<T, I> {

    @Override
    protected PrioritySystem convert(String s) {
        switch (s) {
            case "AUTRE REGIME": return AUTRE_REGIME;
            case "LIVRAISON": return LIVRAISON;
            case "PAYANT ROTATIF": return PAYANT_ROTATIF;
            case "2 ROUES": return DEUX_ROUES;
            case "LOCATION": return LOCATION;
            case "GIG/GIC": return PMR;
            case "PAYANT MIXTE": return PAYANT_MIXTE;
            case "ELECTRIQUE": return ELECTRIQUE;
            case "GRATUIT": return GRATUIT;
            default: return null;
        }
    }
}
