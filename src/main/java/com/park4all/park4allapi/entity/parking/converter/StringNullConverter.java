/**
 * This class will convert the value null if the string is "null"
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.converter;

import com.opencsv.bean.AbstractBeanField;

public class StringNullConverter<T, I> extends AbstractBeanField<T, I> {

    @Override
    protected String convert(String s) {
        if(s.equals("null")) {
            return null;
        } else {
            return s;
        }
    }
}
