/**
 * This class will convert a string to a double
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */


package com.park4all.park4allapi.entity.parking.converter;

import com.opencsv.bean.AbstractBeanField;

public class StringToDoubleConverter<T, I> extends AbstractBeanField<T, I> {

    @Override
    protected Double convert(String s) {
        if(s.equals("null") || s.equals("")) {
            return null;
        } else {
            try {
                return Double.parseDouble(s);
            } catch(Exception e) {
                return null;
            }
        }
    }
}
