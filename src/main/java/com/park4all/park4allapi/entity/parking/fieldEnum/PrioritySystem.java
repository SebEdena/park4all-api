/**
 * This enum contains all possible specificity of a car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.fieldEnum;

public enum PrioritySystem {
    AUTRE_REGIME,
    LIVRAISON,
    PAYANT_ROTATIF,
    DEUX_ROUES,
    LOCATION,
    PMR,
    PAYANT_MIXTE,
    ELECTRIQUE,
    GRATUIT
}
