/**
 * This enum contains all possible type of a car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.parking.fieldEnum;

public enum Type {
    TECHNICAL,
    PUBLIC,
    INDIGO,
    SAEMES,
    INTERPARKING,
    Q_PARK,
    SEIH_ACCOR_ALLSEASONS,
    SAGS,
    REALPARK
}
