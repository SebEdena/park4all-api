/**
 * This enum contains all possible criteria for statistics
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.score.criteria;

public enum Criterion {
    VERTEX_PRICE,
    VERTEX_DISTANCE,
    VERTEX_SPECIFIC_PLACES,
    VERTEX_ALTERNATIVE_PLACES,
    HEURISTIC,
    EDGE_DISTANCE
}
