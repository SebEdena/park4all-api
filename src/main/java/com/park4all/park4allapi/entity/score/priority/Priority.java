/**
 * This enum returns the exacty priority of the car park
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.score.priority;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

public enum Priority {

    @JsonProperty("distance") DISTANCE("distance"),
    @JsonProperty("price") PRICE("price");

    @Getter
    private String name;

    Priority(String name) {
        this.name = name;
    }

    public static Priority getPriority(String name){
        for(Priority priority : Priority.values()){
            if (priority.getName().equals(name)) return priority;
        }

        return null;
    }
}


