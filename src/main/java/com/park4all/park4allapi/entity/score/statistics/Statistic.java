/**
 * This class is for any statistic
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.entity.score.statistics;

import lombok.Data;

@Data
public class Statistic {
    private Double min;
    private Double max;
}
