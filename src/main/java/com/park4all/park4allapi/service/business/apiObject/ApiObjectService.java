/**
 * This interface is implemented by ApiObjectServiceImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.apiObject;

import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;

public interface ApiObjectService {
    ApiParam parseRequestParam(String start, String finish, String vehicle, int radius, String priorities)
            throws ParametersNotValidException;

    ApiResponse initApiResponse(ApiParam apiParam, int code);

    String parseApiResponseToJson(ApiResponse apiResponse) throws ApiRunTimeException;
}
