/**
 * This class implements ApiObjectService
 * This class will conserve all data during the process
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.apiObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.park4all.park4allapi.business.score.priorityParser.PriorityParser;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.vehicle.factory.VehicleFactory;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.score.priority.Priority;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToParseException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service("apiObject")
public class ApiObjectServiceImpl implements ApiObjectService {

    private VehicleFactory vehicleFactory;

    private PriorityParser priorityParser;

    @Value("${param.radius.min}")
    private int minRadius;

    @Value("${param.radius.max}")
    private int maxRadius;

    @Autowired
    public ApiObjectServiceImpl(VehicleFactory vehicleFactory, PriorityParser priorityParser) {
        this.vehicleFactory = vehicleFactory;
        this.priorityParser = priorityParser;
    }

    /**
     * This function will return an API param
     * It will merge all information into an API param
     * @param start
     * @param finish
     * @param vehicle
     * @param radius
     * @param priorities
     * @return
     * @throws ParametersNotValidException
     */
    @Override
    public ApiParam parseRequestParam(String start, String finish, String vehicle, int radius, String priorities)
            throws ParametersNotValidException {

        ApiParam params = new ApiParam();
        String[] tmp;

        try{
            tmp = start.split(",");
            if(tmp.length != 2) throw new IllegalArgumentException();
            Geo startGeo = new Geo();
            startGeo.setLongitude(Double.parseDouble(tmp[0]));
            startGeo.setLatitude(Double.parseDouble(tmp[1]));
            params.setStart(startGeo);
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid coordinates format for parameter [start].");
        }

        try{
            tmp = finish.split(",");
            if(tmp.length != 2) throw new IllegalArgumentException();
            Geo finishGeo = new Geo();
            finishGeo.setLongitude(Double.parseDouble(tmp[0]));
            finishGeo.setLatitude(Double.parseDouble(tmp[1]));
            params.setFinish(finishGeo);
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid coordinates format for parameter [finish].");
        }

        try {
            Vehicle v = vehicleFactory.getVehicle(vehicle);
            params.setVehicle(v);
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid parameter [vehicle]", e.getLocalizedMessage());
        }

        if(radius < minRadius || radius > maxRadius) {
            throw new ParametersNotValidException(String.format("Parameter radius must be between %d and %d", minRadius, maxRadius));
        } else {
            params.setRadius(radius);
        }

        try {
            List<Priority> prioritiesList = priorityParser.parsePriorities(priorities.split(","));
            params.setPriorities(prioritiesList);
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid parameter [priorities]", e.getLocalizedMessage());
        }

        log.info("Query parameters : " + params);

        return params;
    }

    /**
     * This function will initialise an ApiResponse
     * @param apiParam
     * @param code
     * @return
     */
    @Override
    public ApiResponse initApiResponse(ApiParam apiParam, int code) {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setParam(apiParam);
        apiResponse.setCode(code);
        return apiResponse;
    }

    /**
     * This function will parse the ApiResponse into json
     * @param apiResponse
     * @return
     * @throws ApiRunTimeException
     */
    @Override
    public String parseApiResponseToJson(ApiResponse apiResponse) throws ApiRunTimeException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(apiResponse);
        } catch (JsonProcessingException e) {
            throw new FailedToParseException("Unable to parse the answer to JSON.");
        }
    }
}
