/**
 * This interface is implemented by MapboxObjectServiceImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.mapbox.object;

import com.park4all.park4allapi.entity.business.api.mapbox.MapboxParam;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;

public interface MapboxObjectService {
    MapboxParam parseRequestParam(String start, String finish)
            throws ParametersNotValidException;

    MapboxResponse initMapboxResponse(MapboxParam MapboxParam, int code);

    String parseMapboxResponseToJson(MapboxResponse mapboxResponse) throws ApiRunTimeException;
}
