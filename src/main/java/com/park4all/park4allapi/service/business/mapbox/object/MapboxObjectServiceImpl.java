/**
 * This class implements MapboxObjectService
 * This class will conserve all data of Mapbox during the process
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.mapbox.object;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxParam;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToParseException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service("mapboxObject")
public class MapboxObjectServiceImpl implements MapboxObjectService {

    /**
     * This function will return an API param
     * It will merge all information into a MapboxParam
     * @param start
     * @param finish
     * @return
     * @throws ParametersNotValidException
     */
    @Override
    public MapboxParam parseRequestParam(String start, String finish)
            throws ParametersNotValidException {

        MapboxParam params = new MapboxParam();
        String[] tmp;

        try{
            tmp = start.split(",");
            if(tmp.length != 2) throw new IllegalArgumentException();
            Geo startGeo = new Geo();
            startGeo.setLongitude(Double.parseDouble(tmp[0]));
            startGeo.setLatitude(Double.parseDouble(tmp[1]));
            params.setStart(startGeo);
            log.info("Start : " + startGeo.toString());
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid coordinates format for parameter [start].");
        }

        try{
            tmp = finish.split(",");
            if(tmp.length != 2) throw new IllegalArgumentException();
            Geo finishGeo = new Geo();
            finishGeo.setLongitude(Double.parseDouble(tmp[0]));
            finishGeo.setLatitude(Double.parseDouble(tmp[1]));
            params.setFinish(finishGeo);
            log.info("Finish : " + finishGeo.toString());
        } catch (Exception e) {
            throw new ParametersNotValidException("Invalid coordinates format for parameter [finish].");
        }

        return params;
    }

    /**
     * This function will initialise a MapboxResponse
     * @param MapboxParam
     * @param code
     * @return
     */
    @Override
    public MapboxResponse initMapboxResponse(MapboxParam MapboxParam, int code) {
        MapboxResponse mapboxResponse = new MapboxResponse();
        mapboxResponse.setParam(MapboxParam);
        mapboxResponse.setCode(code);
        return mapboxResponse;
    }

    /**
     * This function will parse the MapboxResponse into json
     * @param mapboxResponse
     * @return
     * @throws ApiRunTimeException
     */
    @Override
    public String parseMapboxResponseToJson(MapboxResponse mapboxResponse) throws ApiRunTimeException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(mapboxResponse);
        } catch (JsonProcessingException e) {
            throw new FailedToParseException("Unable to parse the answer to JSON.");
        }
    }
}
