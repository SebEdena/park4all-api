/**
 * The interface MapboxService is implemented by MapboxServiceImpl
 * This class defines all useful functions
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.service.business.mapbox.service;

import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;

import java.io.InputStream;
import java.util.List;

public interface MapboxService {

    void getPath(MapboxResponse mapboxResponse) throws ApiRunTimeException;

    InputStream getPathFromParks(List<Park> carparks) throws ApiRunTimeException;

    InputStream getPathFromGeos(List<Geo> geos) throws ApiRunTimeException;

    InputStream getPathFromStrings(List<String> coordinates) throws ApiRunTimeException;

    String parseArrayToString(List<String> coordinates) throws ApiRunTimeException;

    Answer parseInputStreamToJava(InputStream json) throws ApiRunTimeException;

    String parseInputStreamToString(InputStream json) throws ApiRunTimeException;

    Geo parseCoordinatesRadius(Answer answer, Geo finish, int radius) throws ApiRunTimeException;

    String parseAnswerToString(Answer answer) throws ApiRunTimeException;

}
