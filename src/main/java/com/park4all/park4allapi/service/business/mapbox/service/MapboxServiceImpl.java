/**
 * The class MapboxServiceImpl implements MapboxService
 * This class has most functions to communicate with the MapboxService
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */
package com.park4all.park4allapi.service.business.mapbox.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxParam;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToConnectMapboxException;
import com.park4all.park4allapi.entity.exception.business.FailedToParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("mapboxFetcher")
@PropertySource("classpath:/secret.properties")
public class MapboxServiceImpl implements MapboxService {

    @Value("${mapbox.link}")
    private String link;

    @Value("${mapbox.accessToken}")
    private String accessToken;

    @Value("${mapbox.overview}")
    private String overview;

    @Value("${mapbox.geometry}")
    private String geometry;

    @Value("${mapbox.vehicle}")
    private String vehicle;

    private Distance distance;

    @Autowired
    public MapboxServiceImpl(Distance distance) {
        this.distance = distance;
    }

    /**
     * This function will call the Mapbox service with a MapboxResponse
     * @param mapboxResponse
     * @throws ApiRunTimeException
     */
    @Override
    public void getPath(MapboxResponse mapboxResponse) throws ApiRunTimeException {
        MapboxParam mapboxParam = mapboxResponse.getParam();
        List<Geo> path = new ArrayList<>(Arrays.asList(mapboxParam.getStart(), mapboxParam.getFinish()));
        Answer result = parseInputStreamToJava(getPathFromGeos(path));
        mapboxResponse.setResult(result);
        mapboxResponse.setCarparks(new ArrayList<>());
    }

    /**
     * This function will call the Mapbox service with a list of park translated to a string of coordinates
     * @param carparks
     * @return an InputStream that has a response of the call Mapbox service
     * @throws ApiRunTimeException
     */
    @Override
    public InputStream getPathFromParks(List<Park> carparks) throws ApiRunTimeException {
        List<String> parkGeoStrings = new ArrayList<>();
        for(Park carpark : carparks) {
            Geo geo = carpark.getGeo();
            parkGeoStrings.add(geo.getLongitude() + "," + geo.getLatitude());
        }
        return this.getPathFromStrings(parkGeoStrings);
    }

    /**
     * This function will call the Mapbox service with a list of geo translated to a string of coordinates
     * @param geos
     * @return an InputStream that has a response of the call Mapbox service
     * @throws ApiRunTimeException
     */
    @Override
    public InputStream getPathFromGeos(List<Geo> geos) throws ApiRunTimeException {
        List<String> geoStrings = new ArrayList<>();
        for(Geo geo : geos) {
            geoStrings.add(geo.getLongitude() + "," + geo.getLatitude());
        }
        return this.getPathFromStrings(geoStrings);
    }

    /**
     * This function will call the Mapbox service with a list of coordinates
     * @param coordinates
     * @return an InputStream that has a response of the call Mapbox service
     * @throws ApiRunTimeException
     */
    @Override
    public InputStream getPathFromStrings(List<String> coordinates) throws ApiRunTimeException {
        try{
            String urlString = link + vehicle + "/" + parseArrayToString(coordinates)
                    + "?" + accessToken + "&" + overview + "&" + geometry;
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();
            return connection.getInputStream();
        }catch (IOException e){
            throw new FailedToConnectMapboxException("Failed to connect to Mapbox service");
        }
    }

    /**
     * This function will parse an array of coordinates to a string
     * @param coordinates
     * @return
     * @throws ApiRunTimeException
     */
    @Override
    public String parseArrayToString(List<String> coordinates) throws ApiRunTimeException {
        try{
            StringBuilder result = new StringBuilder();

            for(int counter = 0; counter <= coordinates.size() - 2; counter++){
                result.append(coordinates.get(counter)).append(";");
            }
            return result + coordinates.get(coordinates.size()-1);
        }catch(NullPointerException nullException){
            throw new FailedToParseException("Object Coordinates was null");
        }catch(ArrayIndexOutOfBoundsException indexException){
            throw new FailedToParseException("Not enough coordinates were given");
        }catch(Exception e){
            throw new FailedToParseException("Unknown error");
        }
    }

    /**
     * This function will translate the answer of the Mapbox service to a Java Object
     * @param json
     * @return a java object = Answer
     * @throws ApiRunTimeException
     */
    @Override
    public Answer parseInputStreamToJava(InputStream json) throws ApiRunTimeException{
        ObjectMapper mapper = new ObjectMapper();
        try{
            return mapper.readValue(json, Answer.class);
        }
        catch (IOException e) {
            throw new FailedToParseException("Could not parse json to Java class");
        }catch (NullPointerException e){
            throw new FailedToParseException("Could not parse json to Java class : Null pointer");
        }
    }

    /**
     * This function will translate the Object Answer to a string json
     * @param answer
     * @return a string json
     * @throws ApiRunTimeException
     */
    @Override
    public String parseAnswerToString(Answer answer) throws ApiRunTimeException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(answer);
        }catch (JsonProcessingException e){
            throw new FailedToParseException("Can not parse answer to json");
        }
    }

    /**
     * This function will translate from a mapbox service answer to a string json
     * @param json
     * @return a string json
     * @throws ApiRunTimeException
     */
    @Override
    public String parseInputStreamToString(InputStream json) throws ApiRunTimeException {
        try{
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonMap = mapper.readTree(json);
            return jsonMap.toString();
        }catch(IOException e){
            throw new FailedToParseException("Failed to parse data to json");
        }catch (NullPointerException e){
            throw new FailedToParseException("Failed to parse data to json : Null pointer");
        }
    }

    /**
     * This function will returns the first coordinate in the radius search from the navigation
     * @param answer
     * @param finish
     * @param radius
     * @return the coordinates of the first point
     * @throws ApiRunTimeException
     */
    @Override
    public Geo parseCoordinatesRadius(Answer answer, Geo finish, int radius) throws ApiRunTimeException{
        try{
            List<Double[]> coordinates = answer.getRoutes().get(0).getGeometry().getCoordinates();
            for(int compteur = 0; compteur < coordinates.size(); compteur++){
                Geo tmp = new Geo();
                tmp.setLongitude(coordinates.get(compteur)[0]);
                tmp.setLatitude(coordinates.get(compteur)[1]);
                if(distance.getDistance(tmp, finish) < radius){
                    answer.getRoutes().get(0).getGeometry().setCoordinates(coordinates.subList(0, compteur + 1));
                    return tmp;
                }
            }
            return null;
        }catch(NullPointerException e){
            throw new FailedToParseException("Failed to truncate coordinates : Null pointer");
        }catch (Exception e){
            throw new FailedToParseException("Failed to truncate coordinates : Unknown error");
        }
    }
}
