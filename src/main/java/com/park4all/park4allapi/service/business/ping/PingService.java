/**
 * This interface is implemented by PingServiceImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.ping;

import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;

import javax.servlet.http.HttpServletRequest;

public interface PingService {

    int sendPingRequest(HttpServletRequest request, EnumRequestMethod method, String endPoint) throws ApiRunTimeException;

}
