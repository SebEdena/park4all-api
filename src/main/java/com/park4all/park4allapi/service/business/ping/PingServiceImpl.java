/**
 * This class implements PingService
 * This class will conserve all data for the status of the server
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.ping;

import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.business.net.ServeurURL;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToConnectException;
import com.park4all.park4allapi.entity.exception.business.ServiceNotAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service("pingService")
public class PingServiceImpl implements PingService {

    public PingServiceImpl() {
    }

    /**
     * This function will send the return code
     * @param request
     * @param method
     * @param endPoint
     * @return
     * @throws ApiRunTimeException
     */
    @Override
    public int sendPingRequest(HttpServletRequest request, EnumRequestMethod method, String endPoint) throws ApiRunTimeException {
        try {
            ServeurURL serveur = new ServeurURL("http", request.getLocalName(),
                    request.getLocalPort(), endPoint);

            URL url = new URL(serveur.toString());
            HttpURLConnection connection;
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            connection.connect();
            log.info("Ping status code : " + connection.getResponseCode());
            if (connection.getResponseCode() == HttpStatus.OK.value())
                return connection.getResponseCode();
            else
                throw new ServiceNotAvailableException("Could not reach " + endPoint + ", service not available");
        } catch (IOException ex) {
            throw new FailedToConnectException("Could not connect to server endpoint" + endPoint);
        }
    }
}
