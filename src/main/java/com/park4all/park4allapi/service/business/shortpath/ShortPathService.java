/**
 * This interface is implemented by ShortPathServiceImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */


package com.park4all.park4allapi.service.business.shortpath;

import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;

public interface ShortPathService {

    void computePath(ApiResponse apiResponse) throws ApiRunTimeException;

}
