/**
 * This class implements ShortPathService
 * This class will do the shortest Path
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.business.shortpath;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.business.graph.global.GlobalGraph;
import com.park4all.park4allapi.business.score.compute.ScoreCompute;
import com.park4all.park4allapi.business.score.priorityParser.PriorityParser;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import com.park4all.park4allapi.entity.score.priority.Priority;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.business.mapbox.service.MapboxService;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graphs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service("shortPathService")
public class ShortPathServiceImpl implements ShortPathService {

    @Value("${shortpath.parking.max}")
    private int numberParkingMax;
    private GlobalGraph globalGraph;
    private Distance haversine;
    private PriorityParser priorityParser;
    private ScoreCompute scoreCompute;
    private MapboxService mapboxService;

    @Autowired
    public ShortPathServiceImpl(GlobalGraph globalGraph, Distance haversine, PriorityParser priorityParser, ScoreCompute scoreCompute, MapboxService mapboxService) {
        this.globalGraph = globalGraph;
        this.haversine = haversine;
        this.priorityParser = priorityParser;
        this.scoreCompute = scoreCompute;
        this.mapboxService = mapboxService;
    }

    /**
     * This function will initialise coordinates into a technical park object
     * @param geo
     * @param id
     * @return
     */
    private Park initPark(Geo geo, String id){
        Park park = new Park();
        park.setId(id);
        park.setGeo(geo);
        park.setType(Type.TECHNICAL);
        return park;
    }

    /**
     * This function will process the shortest path in the Api Response
     * @param apiResponse
     * @throws ApiRunTimeException
     */
    @Override
    public void computePath(ApiResponse apiResponse) throws ApiRunTimeException {
        ApiParam params = apiResponse.getParam();
        List<Geo> path = new ArrayList<>(Arrays.asList(params.getStart(),params.getFinish()));
        Answer result = mapboxService.parseInputStreamToJava(mapboxService.getPathFromGeos(path));
        Geo start = mapboxService.parseCoordinatesRadius(result,params.getFinish(),params.getRadius());
        Park startPark = initPark(start, "start");
        Park finishPark = initPark(params.getFinish(), "finish");
        GraphContext graph = globalGraph.createSubGraph(startPark,finishPark,params.getRadius(), params.getVehicle());

        Park parkTo, parkFrom;
        double weightVertex, weightEdge;
        Map<Priority,Integer> priorities = priorityParser.prioritiesToMap(params.getPriorities());

        for (Weight w : graph.getGraph().edgeSet()) {
            parkFrom = graph.getGraph().vertexSet().stream()
                    .filter(park -> park.getId().equals(w.getIdStart()))
                    .collect(Collectors.toList()).get(0);
            parkTo = graph.getGraph().vertexSet().stream()
                    .filter(park -> park.getId().equals(w.getIdFinish()))
                    .collect(Collectors.toList()).get(0);
            weightVertex = scoreCompute.getCarparkScore(parkTo,finishPark,graph, params.getVehicle(), haversine, priorities);
            weightEdge = scoreCompute.getWeightScore(graph.getGraph().getEdge(parkFrom,parkTo),graph);
            graph.getGraph().setEdgeWeight(parkFrom,parkTo,weightEdge+weightVertex);
        }

        List<Park> carparkList = eraseTechnicalParking(getPath(graph, startPark, finishPark));
        if(carparkList.size() > numberParkingMax) {
            carparkList = truncateParking(carparkList, finishPark, graph, params.getVehicle(), priorities);
        }

        Park realStart = initPark(params.getStart(),"realStart");
        List<Park> mapboxParkPath = new ArrayList<>(carparkList);
        mapboxParkPath.add(0, realStart);
        mapboxParkPath.add(mapboxParkPath.size(), finishPark);

        Answer mapboxResult = mapboxService.parseInputStreamToJava(mapboxService.getPathFromParks(mapboxParkPath));
        apiResponse.setResult(mapboxResult);
        apiResponse.setCarparks(params.getVehicle().fillCarparks(carparkList));
    }

    /**
     * This function will return a list of parks that are the best according to the A*
     * @param graph
     * @param startPark
     * @param finishPark
     * @return
     */
    private List<Park> getPath(GraphContext graph, Park startPark, Park finishPark) {
        Map<Park, Park> cameFrom = new HashMap<>();
        Map<Park, Double> weightScore = new HashMap<>();
        Map<Park, Double> heuristicScore = new HashMap<>();

        for(Park park : graph.getGraph().vertexSet()){
            cameFrom.put(park, null);
            weightScore.put(park, park.equals(startPark) ? 0d : Double.MAX_VALUE);
            heuristicScore.put(park, park.equals(startPark)  ? scoreCompute.getHeuristicScore(startPark,finishPark,graph,haversine) : Double.MAX_VALUE);
        }

        Queue<Park> openSet = new PriorityQueue<>((p1, p2) -> Comparator.comparingDouble(park -> heuristicScore.get(park) + weightScore.get(park)).compare(p1, p2));
        openSet.add(startPark);

        while(!openSet.isEmpty()) {
            Park current = openSet.poll();

            if(current.equals(finishPark)) {
                List<Park> path = new LinkedList<>(Collections.singletonList(current));
                while(cameFrom.containsKey(current) && cameFrom.get(current) != null) {
                    current = cameFrom.get(current);
                    path.add(0, current);
                }
                return path;
            }

            for(Park neighbor : Graphs.successorListOf(graph.getGraph(),current)){
                double tmpWeightScore = weightScore.get(current) + graph.getGraph().getEdgeWeight(graph.getGraph().getEdge(current,neighbor));
                if(tmpWeightScore < weightScore.get(neighbor)){
                    cameFrom.put(neighbor, current);
                    weightScore.put(neighbor, tmpWeightScore);
                    heuristicScore.put(neighbor, weightScore.get(neighbor) + scoreCompute.getHeuristicScore(neighbor,finishPark,graph,haversine));

                    if(!openSet.contains(neighbor)) openSet.add(neighbor);
                }
            }
        }

        return new LinkedList<>();
    }

    /**
     * This function will return a subset of best car parks
     * @param parks
     * @param finish
     * @param graphContext
     * @param vehicle
     * @param priorities
     * @return
     */
    private List<Park> truncateParking(List<Park> parks, Park finish, GraphContext graphContext, Vehicle vehicle, Map<Priority, Integer> priorities){
        parks.sort(Comparator.comparingDouble(o -> scoreCompute.getCarparkScore(o, finish, graphContext, vehicle, haversine, priorities)));
        List<Park> sublist = parks.subList(0, numberParkingMax);
        List<Park> result = new ArrayList<>();
        for(Park carpark : parks) {
            if(sublist.contains(carpark)) result.add(carpark);
        }
        return result;
    }

    /**
     * This function will return a subset of car parks without technical
     * @param parkList
     * @return
     */
    private List<Park> eraseTechnicalParking(List<Park> parkList){
        List<Park> response = new ArrayList<>(parkList);
        for(Park p : parkList){
            if (p.getType().equals(Type.TECHNICAL)){
                response.remove(p);
            }
        }
        return response;
    }
}

