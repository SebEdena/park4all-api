/**
 * This interface is implemented by DefaultJSONImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;

public interface DefaultJSON {

    void getAnswer(ApiResponse apiParam, String filePath) throws ApiRunTimeException;

}
