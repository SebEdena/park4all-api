/**
 * This class implements DefaultJSON
 * This class will process to return a default response to the UI
 * This class has been used during the development process
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToParseException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

@Slf4j
@Service("default")
public class DefaultJSONImpl implements DefaultJSON {

    /**
     * This function will prepare the ApiResponse
     * @param apiParam
     * @param filePath
     * @throws ApiRunTimeException
     */
    @Override
    public void getAnswer(ApiResponse apiParam, String filePath) throws ApiRunTimeException {
        try {
            String content = new String(Files.readAllBytes(Paths.get("src/main/resources/json/"+filePath)));
            apiParam.setResult(parseStringToJava(content));
            apiParam.setCarparks(new ArrayList<>());
        } catch (IOException e) {
            throw new FailedToParseException("Unable to find the default JSON return file.");
        }
    }

    /**
     * This function will return an Answer object from a json
     * @param json
     * @return
     * @throws ApiRunTimeException
     */
    private Answer parseStringToJava(String json) throws ApiRunTimeException {
        ObjectMapper mapper = new ObjectMapper();
        try{
            return mapper.readValue(json, Answer.class);
        }
        catch (IOException e) {
            throw new FailedToParseException("Could not parse json to Java class");
        }catch (NullPointerException e){
            throw new FailedToParseException("Could not parse json to Java class : Null pointer");
        }
    }

}
