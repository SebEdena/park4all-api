/**
 * This interface is implemented by PingServiceTestImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;

import javax.servlet.http.HttpServletRequest;

public interface PingServiceTest {

    int sendPingRequest(HttpServletRequest request, EnumRequestMethod method) throws ApiRunTimeException;

    void ping();

}
