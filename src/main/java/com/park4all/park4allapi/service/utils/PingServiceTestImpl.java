/**
 * This class implements PingServiceTest
 * This class will process to return a ping
 * This class has been used during the development process
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.business.net.ServeurURL;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ServiceNotAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.*;

@Slf4j
@Service("pingTest")
public class PingServiceTestImpl implements PingServiceTest {

    public PingServiceTestImpl(){
    }

    /**
     * This function will send the return code
     * @param request
     * @param method
     * @return
     * @throws ApiRunTimeException
     */
    @Override
    public int sendPingRequest(HttpServletRequest request, EnumRequestMethod method) throws ApiRunTimeException {
        try {
            ServeurURL serveur = new ServeurURL("http", InetAddress.getByName(request.getRemoteHost()).getHostAddress(),
                    request.getLocalPort(), "/getPing");

            URL url = new URL(serveur.toString());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            connection.connect();
            return connection.getResponseCode();
        } catch (IOException e) {
            throw new ServiceNotAvailableException("Unable to reach the [/test/ping] service.");
        }
    }

    @Override
    public void ping(){
        log.info("Ping");
    }
}
