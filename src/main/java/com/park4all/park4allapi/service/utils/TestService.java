/**
 * This interface is implemented by TestServiceImpl
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import org.json.JSONObject;

public interface TestService {

    String readJSON(JSONObject param);

}
