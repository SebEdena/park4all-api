/**
 * This class implements PingServiceTest
 * This class will process to test the service
 * This class has been used during the development process
 * @author Mathieu Valentin
 * @author Pierre Verbe
 * @author Sébastien Viguier
 * @author Laurent Yu
 */

package com.park4all.park4allapi.service.utils;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TestServiceImpl implements TestService {

    /**
     * This function will read the default json file or return an error
     * @param param
     * @return
     */
    @Override
    public String readJSON(JSONObject param) {
        log.info("Reading the json file...");
        return param.toString();
    }

}
