package com.park4all.park4allapi;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("com.park4all.park4allapi")
class Park4allApiApplicationTests {

}

