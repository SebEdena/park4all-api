package com.park4all.park4allapi.business.distance;

import com.park4all.park4allapi.entity.parking.bean.Geo;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class HaversineTest {

    private static Geo start;
    private static Geo finish;

    @Autowired
    @Qualifier("haversine")
    private Distance haversine;

    @BeforeAll
    public static void setUp() {
        start = new Geo();
        start.setLatitude(48.880834);
        start.setLongitude(2.362338);

        finish = new Geo();
        finish.setLatitude(48.855821);
        finish.setLongitude(2.3785623);
    }

    @Test
    void getDistance() {
        assertEquals(3024, Math.round(haversine.getDistance(start, finish)));
    }
}
