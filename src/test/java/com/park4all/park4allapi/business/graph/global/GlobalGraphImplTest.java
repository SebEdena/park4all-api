package com.park4all.park4allapi.business.graph.global;

import com.park4all.park4allapi.business.graph.global.GlobalGraph;
import com.park4all.park4allapi.business.utils.csv.ParkingParser;
import com.park4all.park4allapi.business.utils.graph.GraphUtils;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.entity.parking.bean.Park;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GlobalGraphImplTest {

    @Autowired
    private GlobalGraph graph;

    @Autowired
    private GraphUtils graphUtils;

    @Autowired
    private ParkingParser parkingParser;

    @Autowired
    @Qualifier("car")
    private Vehicle vehicle;

    @Test
    void createSubGraph() throws IOException {
        //BEFORE
        String resource = "/csv/carparks.csv";
        Map<String, Park> parks = parkingParser.parseParkings(resource);
        graphUtils.initGraph(graph.getGraph(), parks, new ArrayList<>());

        List<Park> parkList = new ArrayList<>(graph.getGraph().vertexSet());
        GraphContext subGraph = graph.createSubGraph(parkList.get(0), parkList.get(1000), 50, vehicle);

        assertNotEquals(0, subGraph.getGraph().edgesOf(parkList.get(0)).size());
        assertNotEquals(0, subGraph.getGraph().edgesOf(parkList.get(1000)).size());
        assertNotNull(subGraph.getGraph());
    }
}