package com.park4all.park4allapi.business.score.compute;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.score.compute.testClasses.MockDistance;
import com.park4all.park4allapi.business.score.compute.testClasses.MockVehicule;
import com.park4all.park4allapi.business.score.priorityParser.PriorityParser;
import com.park4all.park4allapi.business.utils.csv.ParkingParser;
import com.park4all.park4allapi.business.utils.graph.GraphUtils;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.business.graph.context.GraphContextImpl;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.score.priority.Priority;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ScoreComputeImplTest {
    private Distance distance;
    private Vehicle vehicle;
    private GraphContext graphContext;

    @Autowired
    private PriorityParser priorityParser;

    @Autowired
    private ParkingParser parkingParser;

    @Autowired
    private GraphUtils graphUtils;

    @Autowired
    private ScoreCompute scoreCompute;

    private Map<Priority, Integer> priorityMap;

    @BeforeAll
     void init() throws IOException {
        distance = new MockDistance();
        vehicle = new MockVehicule();
        graphContext = new GraphContextImpl();

        String[] tabPriorities = {"price","distance"};
        priorityMap = priorityParser.prioritiesToMap(priorityParser.parsePriorities(tabPriorities));

        Map<String, Park> parkings = parkingParser.parseParkings("/csv/test/score/carparks.csv");
        List<Weight> weights = parkingParser.parseWeights("/csv/test/score/graphlinks.csv");

        Graph<Park, Weight> graph = graphUtils.initEmptyGraph();
        graphUtils.initGraph(graph, parkings, weights);
        graphContext.setGraph(graph);

        graphContext.fillStatistics(vehicle, 10);

        /*log.info("VERTEX_PRICE : " + graphContext.getStatistic(VERTEX_PRICE));
        log.info("VERTEX_DISTANCE : " + graphContext.getStatistic(VERTEX_DISTANCE));
        log.info("VERTEX_SPECIFIC_PLACES : " + graphContext.getStatistic(VERTEX_SPECIFIC_PLACES));
        log.info("VERTEX_ALTERNATIVE_PLACES : " + graphContext.getStatistic(VERTEX_ALTERNATIVE_PLACES));
        log.info("HEURISTIC : " + graphContext.getStatistic(HEURISTIC));
        log.info("EDGE_DISTANCE : " + graphContext.getStatistic(EDGE_DISTANCE));*/
    }

    @Test
    void getCarparkScoreTest() {
        Park start = graphContext.getGraph().vertexSet().stream().filter((park) -> park.getId().equals("t3")).findFirst().get();
        Park destination = graphContext.getGraph().vertexSet().stream().filter((park) -> park.getId().equals("t1")).findFirst().get();

        Double score = scoreCompute.getCarparkScore(start, destination, graphContext, vehicle, distance, priorityMap);
        assertEquals(2d/3d + 0.05 + 0.5 + 1d/6d, score);
    }

    @Test
    void getWeightScoreTest() {
        Weight weight = graphContext.getGraph().edgeSet().stream().filter((park) -> park.getIdStart().equals("t2") && park.getIdFinish().equals("t3")).findFirst().get();

        Double scoreWeight = scoreCompute.getWeightScore(weight, graphContext);
        assertEquals(1d/3d, scoreWeight);
    }

    @Test
    void getHeuristicScoreTest() {
        Park start = graphContext.getGraph().vertexSet().stream().filter((park) -> park.getId().equals("t1")).findFirst().get();
        Park destination = graphContext.getGraph().vertexSet().stream().filter((park) -> park.getId().equals("t2")).findFirst().get();

        Double scoreHeuristique = scoreCompute.getHeuristicScore(start, destination, graphContext, distance);
        assertEquals(0.4, scoreHeuristique);
    }
}