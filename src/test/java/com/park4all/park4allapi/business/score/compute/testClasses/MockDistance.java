package com.park4all.park4allapi.business.score.compute.testClasses;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.entity.parking.bean.Geo;

public class MockDistance implements Distance {

    @Override
    public double getDistance(Geo start, Geo finish) {
        return 1;
    }
}
