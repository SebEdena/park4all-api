package com.park4all.park4allapi.business.score.compute.testClasses;

import com.park4all.park4allapi.business.vehicle.impl.AbstractVehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;

import java.util.function.Predicate;

public class MockVehicule extends AbstractVehicle {

    @Override
    public String getName() {
        return "mockVehicle";
    }

    @Override
    public Predicate<Park> getFilter() {
        return (park) -> true;
    }

    @Override
    public double getPrice(Park carpark) {
        return carpark.getPrice1Hour();
    }

    @Override
    public int getSpecificPlaces(Park carpark) {
        return carpark.getTotalPark();
    }

    @Override
    public int getAlternativePlaces(Park carpark) {
        return carpark.getParkMotorcycle();
    }

}
