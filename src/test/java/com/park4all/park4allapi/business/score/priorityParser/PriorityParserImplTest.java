package com.park4all.park4allapi.business.score.priorityParser;

import com.park4all.park4allapi.entity.score.priority.Priority;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PriorityParserImplTest {

    @Autowired
    private PriorityParser priorityParser;

    @Test
    void parsePrioritiesTest() {
        String[] tabPriorities = {"price","distance"};

        List<Priority> resultList = priorityParser.parsePriorities(tabPriorities);

        assertEquals(Priority.PRICE, resultList.get(0));
        assertEquals(Priority.DISTANCE, resultList.get(1));

        String[] tabPriorities2 = {"price","c'estnullleeee"};
        assertThrows(IllegalArgumentException.class, () -> priorityParser.parsePriorities(tabPriorities2));

        String[] tabPriorities3 = {"price","price"};
        assertThrows(IllegalArgumentException.class, () -> priorityParser.parsePriorities(tabPriorities3));

        String[] tabPriorities4 = {"price"};
        assertThrows(IllegalArgumentException.class, () -> priorityParser.parsePriorities(tabPriorities4));
    }

    @Test
    void prioritiesToMapTest() {
        List<Priority> priorities = Arrays.asList(Priority.DISTANCE, Priority.PRICE);

        Map<Priority, Integer> resultMap = priorityParser.prioritiesToMap(priorities);

        assertEquals(0, resultMap.get(Priority.DISTANCE));
        assertEquals(1, resultMap.get(Priority.PRICE));
    }
}