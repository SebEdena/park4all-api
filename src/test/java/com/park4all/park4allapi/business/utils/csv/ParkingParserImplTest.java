package com.park4all.park4allapi.business.utils.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderHeaderAwareBuilder;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ParkingParserImplTest {

    @Autowired
    private ParkingParser parkingParser;

    private int countRows(String path) throws Exception {
        FileReader file = new FileReader(new ClassPathResource(path).getFile());
        CSVParser csvParser = new CSVParserBuilder().withSeparator(';').build();
        CSVReader csvReader = new CSVReaderHeaderAwareBuilder(file).withCSVParser(csvParser).build();
        int result = csvReader.readAll().size();
        csvReader.close();
        file.close();
        return result;
    }

    @Test
    public void parseParkingsCountTest() throws Exception {
        String resource = "/csv/carparks.csv";
        int theoreticalCount = countRows(resource);
        Map<String, Park> parks = parkingParser.parseParkings(resource);
        assertEquals(theoreticalCount, parks.size());
    }

    @Test
    public void parseWeightsTest() throws IOException {
        String resource = "/csv/graphlinks.csv";
        List<Weight> weights = parkingParser.parseWeights(resource);
        for(Weight weight : weights){
            assertTrue(weight.isCorrectWeight());
        }
    }

}