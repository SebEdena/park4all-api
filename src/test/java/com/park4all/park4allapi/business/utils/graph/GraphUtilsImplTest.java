package com.park4all.park4allapi.business.utils.graph;

import com.park4all.park4allapi.business.utils.csv.ParkingParser;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import org.jgrapht.Graph;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GraphUtilsImplTest {

    @Autowired
    private GraphUtils graphUtils;

    @Autowired
    private ParkingParser parkingParser;

    @Test
    public void initGraphTest() throws IOException {
        String resource = "/csv/carparks.csv";
        Map<String, Park> carparks = parkingParser.parseParkings(resource);

        String resourceWeight = "/csv/graphlinks.csv";
        List<Weight> weights = parkingParser.parseWeights(resourceWeight);

        Graph<Park, Weight> graph = graphUtils.initEmptyGraph();
        graphUtils.initGraph(graph, carparks, weights);

        assertEquals(carparks.size(), graph.vertexSet().size());
    }

    @Test
    public void countSubGraphTest() throws IOException {
        String resource = "/csv/carparks.csv";
        Map<String, Park> carparks = parkingParser.parseParkings(resource);

        String resourceWeight = "/csv/graphlinks.csv";
        List<Weight> weights = parkingParser.parseWeights(resourceWeight);

        Graph<Park, Weight> graph = graphUtils.initEmptyGraph();
        graphUtils.initGraph(graph, carparks, weights);

        assertNotEquals(1, graphUtils.countSubGraph(graph));
        assertNotEquals(0, graph.vertexSet().size());
        assertNotEquals(0, graph.edgeSet().size());
    }
}