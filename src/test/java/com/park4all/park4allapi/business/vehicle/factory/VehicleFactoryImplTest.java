package com.park4all.park4allapi.business.vehicle.factory;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.business.vehicle.impl.Car;
import com.park4all.park4allapi.business.vehicle.impl.Electric;
import com.park4all.park4allapi.business.vehicle.impl.Motorcycle;
import com.park4all.park4allapi.business.vehicle.impl.Prm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class VehicleFactoryImplTest {

    @Autowired
    VehicleFactory factory;

    @Test
    public void getVehicleTest() {
        Vehicle v;

        v = factory.getVehicle("car");
        assertEquals(v.getClass(), Car.class);

        v = factory.getVehicle("motorcycle");
        assertEquals(v.getClass(), Motorcycle.class);

        v = factory.getVehicle("electric");
        assertEquals(v.getClass(), Electric.class);

        v = factory.getVehicle("prm");
        assertEquals(v.getClass(), Prm.class);
    }
}