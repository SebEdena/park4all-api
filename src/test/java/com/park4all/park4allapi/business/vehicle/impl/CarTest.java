package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CarTest {

    @Autowired
    @Qualifier("car")
    private Vehicle car;

    @Test
    public void testCorrectFilter() {
        Park bean = new Park();
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);

        //Everything correct
        assertTrue(car.getFilter().test(bean));

        //Incorrect prioritySystem
        bean.setPrioritySystem(PrioritySystem.DEUX_ROUES);
        assertFalse(car.getFilter().test(bean));

        //Not public parking
        bean.setType(Type.INDIGO);
        assertTrue(car.getFilter().test(bean));

        //No price has been specified
        bean.setPrice1Hour(null);
        assertFalse(car.getFilter().test(bean));
        bean.setPrice1Hour(0d);

        //Not car compatible parking
        bean.setCar(false);
        assertFalse(car.getFilter().test(bean));
    }

    @Test
    public void getPriceTest() {
        double value = 2d;
        Park bean = new Park();

        bean.setPrice1Hour(value);
        bean.setType(Type.PUBLIC);
        assertEquals(value, car.getPrice(bean));

        bean.setPrioritySystem(PrioritySystem.GRATUIT);
        assertEquals(0d, car.getPrice(bean));

        bean.setType(Type.SAEMES);
        assertEquals(value, car.getPrice(bean));
    }

    @Test
    void getSpecificPlaces() {
        Park bean = new Park();
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
        bean.setTotalPark(4);

        assertEquals( 4, car.getSpecificPlaces(bean));

        bean.setPrioritySystem(PrioritySystem.PMR);
        assertEquals( 0, car.getSpecificPlaces(bean));

        bean.setType(Type.INDIGO);
        assertEquals( 4, car.getSpecificPlaces(bean));
    }

    @Test
    void getAlternativePlaces() {
        Park bean = new Park();
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
        assertEquals(0, car.getAlternativePlaces(bean));
    }
}