package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class ElectricTest {

    @Autowired
    @Qualifier("electric")
    private Vehicle electricCar;

    @Test
    public void testCorrectFilter() {
        Park bean = new Park();
        bean.setCar(true);
        //bean.setElectricalCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.ELECTRIQUE);

        //Everything correct
        assertTrue(electricCar.getFilter().test(bean));

        //Incorrect prioritySystem
        bean.setPrioritySystem(PrioritySystem.DEUX_ROUES);
        assertFalse(electricCar.getFilter().test(bean));

        //Not public parking
        bean.setType(Type.INDIGO);
        assertTrue(electricCar.getFilter().test(bean));

        //No price has been specified
        bean.setPrice1Hour(null);
        assertFalse(electricCar.getFilter().test(bean));
        bean.setPrice1Hour(0d);

        //Not electrical car compatible parking
        //bean.setElectricalCar(false);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PMR);
        assertFalse(electricCar.getFilter().test(bean));
    }

    @Test
    public void getPriceTest() {
        double value = 2d;
        Park bean = new Park();
        bean.setPrice1Hour(value);
        assertEquals(value, electricCar.getPrice(bean));
    }

    @Test
    void getSpecificPlaces() {
        Park bean = new Park();
        bean.setElectricalCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.ELECTRIQUE);
        bean.setTotalPark(4);

        assertEquals( 4, electricCar.getSpecificPlaces(bean));

        bean.setPrioritySystem(PrioritySystem.PMR);
        assertEquals( 0, electricCar.getSpecificPlaces(bean));

        bean.setType(Type.INDIGO);
        assertEquals( 1, electricCar.getSpecificPlaces(bean));
    }

    @Test
    void getAlternativePlaces() {
        Park bean = new Park();
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.ELECTRIQUE);
        assertEquals(0, electricCar.getAlternativePlaces(bean));
    }
}
