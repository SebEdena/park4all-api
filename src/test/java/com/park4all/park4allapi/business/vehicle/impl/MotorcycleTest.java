package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MotorcycleTest {

    @Autowired
    @Qualifier("motorcycle")
    private Vehicle kawasaki_H2R;

    @Test
    public void testCorrectFilter(){
        Park bean = new Park();

        // #### Test with motorcycle only ####
        bean.setMotorcycle(true);
        bean.setPrice1HourMotorcycle(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.DEUX_ROUES);

        //Everything correct
        assertTrue(kawasaki_H2R.getFilter().test(bean));

        //Incorrect prioritySystem
        bean.setPrioritySystem(PrioritySystem.PMR);
        assertFalse(kawasaki_H2R.getFilter().test(bean));

        //Not public parking
        bean.setType(Type.INDIGO);
        assertTrue(kawasaki_H2R.getFilter().test(bean));

        //No price has been specified
        bean.setPrice1HourMotorcycle(null);
        assertFalse(kawasaki_H2R.getFilter().test(bean));
        bean.setPrice1HourMotorcycle(0d);

        //Not motorcycle compatible parking
        bean.setMotorcycle(false);
        assertFalse(kawasaki_H2R.getFilter().test(bean));

        // #### Test with both motorcycle and car #####
        bean.setMotorcycle(true);
        bean.setCar(true);
        bean.setPrice1HourMotorcycle(0d);
        bean.setPrice1Hour(1d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_ROTATIF);

        // Everything correct
        assertTrue(kawasaki_H2R.getFilter().test(bean));

        //Incorrect prioritySystem
        bean.setPrioritySystem(PrioritySystem.PMR);
        assertFalse(kawasaki_H2R.getFilter().test(bean));

        //Not public parking
        bean.setType(Type.INDIGO);
        assertTrue(kawasaki_H2R.getFilter().test(bean));

        //No price has been specified for motorcycle
        bean.setPrice1HourMotorcycle(null);
        assertNotNull(bean.getPrice1Hour());
        assertEquals(bean.getPrice1Hour(),1d);
        assertTrue(kawasaki_H2R.getFilter().test(bean));
        bean.setPrice1HourMotorcycle(0d);

        //No price has been specified for car
        bean.setPrice1Hour(null);
        assertNotNull(bean.getPrice1HourMotorcycle());
        assertEquals(bean.getPrice1HourMotorcycle(),0d);
        assertTrue(kawasaki_H2R.getFilter().test(bean));
        bean.setPrice1Hour(1d);

        //Not motorcycle compatible parking
        bean.setMotorcycle(false);
        assertTrue(kawasaki_H2R.getFilter().test(bean));
        bean.setMotorcycle(true);

        //Not car compatible parking
        bean.setCar(false);
        assertTrue(kawasaki_H2R.getFilter().test(bean));

        //Not motorcycle and car compatible parking
        bean.setMotorcycle(false);
        assertFalse(kawasaki_H2R.getFilter().test(bean));
    }

    @Test
    void testGetPrice(){
        Park bean = new Park();
        bean.setMotorcycle(true);
        bean.setPrice1HourMotorcycle(0d);
        bean.setPrice1Hour(2d);

        assertEquals(kawasaki_H2R.getPrice(bean),0d);

        bean.setMotorcycle(false);
        bean.setPrice1HourMotorcycle(null);
        bean.setCar(true);

        assertEquals(kawasaki_H2R.getPrice(bean),2d);
    }

    @Test
    void getSpecificPlaces() {
        Park bean = new Park();
        bean.setMotorcycle(true);
        bean.setPrice1HourMotorcycle(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.DEUX_ROUES);
        bean.setTotalPark(4);

        assertEquals( 4, kawasaki_H2R.getSpecificPlaces(bean));

        bean.setPrioritySystem(PrioritySystem.PMR);
        assertEquals( 0, kawasaki_H2R.getSpecificPlaces(bean));

        bean.setType(Type.INDIGO);
        assertEquals( 4, kawasaki_H2R.getSpecificPlaces(bean));
    }

    @Test
    void getAlternativePlaces() {
        Park bean = new Park();
        bean.setMotorcycle(true);
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setPrice1HourMotorcycle(1d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
        assertEquals(0, kawasaki_H2R.getAlternativePlaces(bean));
    }
}