package com.park4all.park4allapi.business.vehicle.impl;

import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.fieldEnum.PrioritySystem;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PrmTest {

    @Autowired
    @Qualifier("prm")
    private Vehicle car;

    static Park bean;

    @BeforeEach
    void setUp() {
        bean = new Park();
        bean.setCar(true);
        bean.setPrice1Hour(0d);
        bean.setType(Type.PUBLIC);
        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
    }

    @Test
    public void testCorrectFilter() {
        //Everything correct
        assertTrue(car.getFilter().test(bean));

        //Incorrect prioritySystem
        bean.setPrioritySystem(PrioritySystem.DEUX_ROUES);
        assertFalse(car.getFilter().test(bean));

        //Not public parking
        bean.setType(Type.INDIGO);
        bean.setSurfaceLift(true);
        assertTrue(car.getFilter().test(bean));

        //No price has been specified
        bean.setPrice1Hour(null);
        assertTrue(car.getFilter().test(bean));
        bean.setPrice1Hour(0d);

        //Not car compatible parking
        bean.setCar(false);
        assertFalse(car.getFilter().test(bean));
    }

    @Test
    public void getPriceTest(){
        assertEquals(0, car.getPrice(bean));

        bean.setType(Type.INDIGO);
        bean.setPrice1Hour(8d);
        assertEquals(8d, car.getPrice(bean));
    }

    @Test
    public void getSpecificPlacesTest(){
        bean.setPrioritySystem(PrioritySystem.PMR);
        bean.setTotalPark(3);
        assertEquals(3, car.getSpecificPlaces(bean));

        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
        assertEquals(0, car.getSpecificPlaces(bean));

        bean.setType(Type.INDIGO);
        bean.setParkPMR(3);
        assertEquals(3, car.getSpecificPlaces(bean));
    }

    @Test
    public void getAlternativePlacesTest(){
        bean.setPrioritySystem(PrioritySystem.PMR);
        bean.setTotalPark(3);
        assertEquals(0, car.getAlternativePlaces(bean));

        bean.setPrioritySystem(PrioritySystem.PAYANT_MIXTE);
        assertEquals(3, car.getAlternativePlaces(bean));

        bean.setType(Type.INDIGO);
        bean.setParkPMR(3);
        assertEquals(3, car.getAlternativePlaces(bean));
    }
}