package com.park4all.park4allapi.entity.business.api;

import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class ApiResponseTest {

    @Test
    void testSetCode() {
        ApiResponse apiResponse = new ApiResponse();
        HttpStatus expected = HttpStatus.OK;
        apiResponse.setCode(HttpStatus.OK.value());
        assertEquals(expected.value(),apiResponse.getCode());
        assertEquals(expected.getReasonPhrase(),apiResponse.getStatus());
        assertEquals(expected.toString(),apiResponse.getCode()+" "+apiResponse.getStatus());
    }

}