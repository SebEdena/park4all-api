package com.park4all.park4allapi.entity.business.net;

import com.park4all.park4allapi.entity.business.net.ServeurURL;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ServeurURLTest {

    private ServeurURL serveur;

    @Test
    void testProperties(){
        String scheme = "http", host = "localhost", service = "/service";
        int port = 5320;
        serveur = new ServeurURL(scheme,host,port,service);
        assertEquals(scheme,serveur.getScheme());
        assertEquals(host,serveur.getHost());
        assertEquals(port,serveur.getPort());
        assertEquals(service,serveur.getService());
    }

    @Test
    void testToString() {
        String expected = "http://[0:0:0:0:0:0:0:1]:8080/service";
        serveur = new ServeurURL("http", "0:0:0:0:0:0:0:1", 8080, "/service");
        assertEquals(expected, serveur.toString());

        expected = "http://localhost:8080/service";
        serveur = new ServeurURL("http", "localhost", 8080, "/service");
        assertEquals(expected, serveur.toString());
    }
}