package com.park4all.park4allapi.entity.exception.business;

import com.park4all.park4allapi.entity.exception.utils.MyException;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.*;

class ApiRunTimeExceptionTest {

    private static String MESSAGE = "An error is thrown";

    @Test
    void handleMyException() {
        try {
            throw new MyException(MESSAGE);
        } catch (MyException ex){
            assertEquals("Exception due to MyException",ex.getApiError().getMessage());
            assertEquals(MESSAGE, ex.getApiError().getErrors().get(0));
            assertEquals(HttpStatus.resolve(500), ex.getApiError().getStatus());
        }
    }

    @Test
    void handleServiceNotAvailableException() {
        try {
            throw new ServiceNotAvailableException(MESSAGE);
        } catch (ServiceNotAvailableException ex){
            assertEquals("Service not available",ex.getApiError().getMessage());
            assertEquals(MESSAGE, ex.getApiError().getErrors().get(0));
            assertEquals(HttpStatus.resolve(503), ex.getApiError().getStatus());
        }
    }

    @Test
    void handleFailedToConnectException() {
        try {
            throw new FailedToConnectException(MESSAGE);
        } catch (FailedToConnectException ex){
            assertEquals("Could not connect to the server",ex.getApiError().getMessage());
            assertEquals(MESSAGE, ex.getApiError().getErrors().get(0));
            assertEquals(HttpStatus.resolve(500), ex.getApiError().getStatus());
        }
    }

    @Test
    void handleFailedToConnectMapboxException() {
        try {
            throw new FailedToConnectMapboxException(MESSAGE);
        } catch (FailedToConnectMapboxException ex){
            assertEquals("Could not connect to Mapbox Server",ex.getApiError().getMessage());
            assertEquals(MESSAGE, ex.getApiError().getErrors().get(0));
            assertEquals(HttpStatus.resolve(500), ex.getApiError().getStatus());
        }
    }

    @Test
    void handleFailedToParseException() {
        try {
            throw new FailedToParseException(MESSAGE);
        } catch (FailedToParseException ex){
            assertEquals("Could not parse the data",ex.getApiError().getMessage());
            assertEquals(MESSAGE, ex.getApiError().getErrors().get(0));
            assertEquals(HttpStatus.resolve(500), ex.getApiError().getStatus());
        }
    }
}