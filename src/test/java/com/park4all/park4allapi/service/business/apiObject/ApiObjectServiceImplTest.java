package com.park4all.park4allapi.service.business.apiObject;

import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.score.priority.Priority;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ApiObjectServiceImplTest {

    @Autowired
    private ApiObjectService apiObjectService;

    @Test
    void parseRequestParamTest() throws ParametersNotValidException {

        assertDoesNotThrow(() -> {
            apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,price");
        });

        ApiParam apiParam = apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,price");

        assertEquals(0, apiParam.getStart().getLongitude());
        assertEquals(2, apiParam.getStart().getLatitude());
        assertEquals(0, apiParam.getFinish().getLongitude());
        assertEquals(2, apiParam.getFinish().getLatitude());
        assertEquals("car", apiParam.getVehicle().getName());
        assertEquals(200, apiParam.getRadius());
        assertEquals(Priority.DISTANCE, apiParam.getPriorities().get(0));
        assertEquals(Priority.PRICE, apiParam.getPriorities().get(1));

        //Start errors
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2,3", "0,2", "car", 200, "distance,price"));
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,a", "0,2", "car", 200, "distance,price"));

        //Finish errors
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2,3", "car", 200, "distance,price"));
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,a", "car", 200, "distance,price"));

        //Vehicle error
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "ca", 200, "distance,price"));

        //Radius errors
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "car", 25, "distance,price"));
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "car", 600, "distance,price"));

        //Priorities errors
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,distance"));
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,foo"));
        assertThrows(ParametersNotValidException.class,
                () -> apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance"));
    }

    @Test
    void initApiResponseTest() throws ParametersNotValidException {
        ApiParam apiParam = apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,price");
        ApiResponse apiResponse = apiObjectService.initApiResponse(apiParam, 200);

        assertEquals(200, apiParam.getRadius());
        assertEquals(200, apiResponse.getCode());
        assertEquals("OK", apiResponse.getStatus());
    }

    @Test
    void parseApiResponseToJsonTest() throws ApiRunTimeException {

        String param = "{\"priorities\":[\"distance\",\"price\"],\"start\":{\"latitude\":2,\"longitude\":0},\"finish\":{\"latitude\":2,\"longitude\":0},\"radius\":200,\"vehicle\":\"car\"}";
        String answer = "{\"routes\":null,\"code\":null,\"uuid\":null,\"waypoints\":null}";
        String carparks = "[]";

        ApiParam apiParam = apiObjectService.parseRequestParam("0,2", "0,2", "car", 200, "distance,price");
        ApiResponse apiResponse = apiObjectService.initApiResponse(apiParam, 200);
        apiResponse.setCarparks(new ArrayList<>());
        apiResponse.setResult(new Answer());

        String json = apiObjectService.parseApiResponseToJson(apiResponse);
        JSONObject jsonObject = new JSONObject(json);

        assertEquals(200, jsonObject.get("code"));
        assertEquals("OK", jsonObject.get("status"));
        assertEquals(param, jsonObject.get("param").toString());
        assertEquals(answer, jsonObject.get("result").toString());
        assertEquals(carparks, jsonObject.get("carparks").toString());

    }
}