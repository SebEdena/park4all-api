package com.park4all.park4allapi.service.business.mapbox.object;

import com.park4all.park4allapi.entity.business.api.mapbox.MapboxParam;
import com.park4all.park4allapi.entity.business.api.mapbox.MapboxResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.ParametersNotValidException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MapboxObjectServiceImplTest {

    @Autowired
    private MapboxObjectService mapboxObjectService;

    @Test
    void parseRequestParamTest() throws ParametersNotValidException {

        assertDoesNotThrow(() -> {
            mapboxObjectService.parseRequestParam("0,2", "0,2");
        });

        MapboxParam mapboxParam = mapboxObjectService.parseRequestParam("0,2", "0,2");

        assertEquals(0, mapboxParam.getStart().getLongitude());
        assertEquals(2, mapboxParam.getStart().getLatitude());
        assertEquals(0, mapboxParam.getFinish().getLongitude());
        assertEquals(2, mapboxParam.getFinish().getLatitude());

        //Start errors
        assertThrows(ParametersNotValidException.class,
                () -> mapboxObjectService.parseRequestParam("0,2,3", "0,2"));
        assertThrows(ParametersNotValidException.class,
                () -> mapboxObjectService.parseRequestParam("0,a", "0,2"));

        //Finish errors
        assertThrows(ParametersNotValidException.class,
                () -> mapboxObjectService.parseRequestParam("0,2", "0,2,3"));
        assertThrows(ParametersNotValidException.class,
                () -> mapboxObjectService.parseRequestParam("0,2", "0,a"));
    }

    @Test
    void initMapboxResponseTest() throws ParametersNotValidException {
        MapboxParam mapboxParam = mapboxObjectService.parseRequestParam("0,2", "0,2");
        MapboxResponse mapboxResponse = mapboxObjectService.initMapboxResponse(mapboxParam, 200);

        assertEquals(0, mapboxParam.getStart().getLongitude());
        assertEquals(200, mapboxResponse.getCode());
        assertEquals("OK", mapboxResponse.getStatus());
    }

    @Test
    void parseMapboxResponseToJsonTest() throws ApiRunTimeException {

        String param = "{\"start\":{\"latitude\":2,\"longitude\":0},\"finish\":{\"latitude\":2,\"longitude\":0}}";
        String answer = "{\"routes\":null,\"code\":null,\"uuid\":null,\"waypoints\":null}";
        String carparks = "[]";

        MapboxParam mapboxParam = mapboxObjectService.parseRequestParam("0,2", "0,2");
        MapboxResponse mapboxResponse = mapboxObjectService.initMapboxResponse(mapboxParam, 200);
        mapboxResponse.setCarparks(new ArrayList<>());
        mapboxResponse.setResult(new Answer());

        String json = mapboxObjectService.parseMapboxResponseToJson(mapboxResponse);
        JSONObject jsonObject = new JSONObject(json);

        assertEquals(200, jsonObject.get("code"));
        assertEquals("OK", jsonObject.get("status"));
        assertEquals(param, jsonObject.get("param").toString());
        assertEquals(answer, jsonObject.get("result").toString());
        assertEquals(carparks, jsonObject.get("carparks").toString());

    }
}