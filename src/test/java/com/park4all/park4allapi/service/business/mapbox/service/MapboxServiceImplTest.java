package com.park4all.park4allapi.service.business.mapbox.service;

import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToParseException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class MapboxServiceImplTest {

    @Autowired
    private MapboxService mapbox;

    private static List<String> coordinates;

    @BeforeAll
    public static void setUp(){
        coordinates = new ArrayList<>();
        coordinates.add("2.362465,48.7");
        coordinates.add("2.380894,48.855649");
    }

    @Test
    public void parseArrayToStringTest() throws ApiRunTimeException{
        assertEquals("2.362465,48.7;2.380894,48.855649", mapbox.parseArrayToString(coordinates));
        coordinates.add("2.380894,48.855649");
        assertEquals("2.362465,48.7;2.380894,48.855649;2.380894,48.855649",mapbox.parseArrayToString(coordinates));
        coordinates.add("2.380894,48.855649");
        assertEquals("2.362465,48.7;2.380894,48.855649;2.380894,48.855649;2.380894,48.855649",mapbox.parseArrayToString(coordinates));
    }

    @Test
    public void getPathTest() throws ApiRunTimeException {
        InputStream ans = mapbox.getPathFromStrings(coordinates);
        assertNotNull(ans);

        assertThrows(FailedToParseException.class, () -> mapbox.getPathFromStrings(new ArrayList<>()));

        assertThrows(FailedToParseException.class, () -> mapbox.getPathFromStrings(null));
    }

    @Test
    public void parseToJavaTest() throws ApiRunTimeException {
        InputStream inputStream = mapbox.getPathFromStrings(coordinates);
        Answer answer = mapbox.parseInputStreamToJava(inputStream);
        assertEquals("Ok", answer.getCode());

        assertThrows(FailedToParseException.class, () -> mapbox.parseInputStreamToJava(inputStream));

        assertThrows(FailedToParseException.class, () -> mapbox.parseInputStreamToJava(null));
    }

    @Test
    public void parseToStringTest() throws ApiRunTimeException {
        InputStream inputStream = mapbox.getPathFromStrings(coordinates);
        Answer answer = mapbox.parseInputStreamToJava(inputStream);
        String response = mapbox.parseAnswerToString(answer);
        System.out.println(response);
        assertNotNull(response);

        assertThrows(FailedToParseException.class, () -> mapbox.parseInputStreamToString(inputStream));

        assertThrows(FailedToParseException.class, () -> mapbox.parseInputStreamToString(null));
    }

    @Test
    public void parseCoordinatesRadius() throws ApiRunTimeException{
        InputStream inputStream = mapbox.getPathFromStrings(coordinates);
        Answer answer = mapbox.parseInputStreamToJava(inputStream);
        int size = answer.getRoutes().get(0).getGeometry().getCoordinates().size();
        System.out.println("Taille réelle : " + size);
        Geo finish = new Geo();
        finish.setLongitude(2.380894);
        finish.setLatitude(48.855649);
        mapbox.parseCoordinatesRadius(answer, finish, 300);
        System.out.println("Taille truncate : " + answer.getRoutes().get(0).getGeometry().getCoordinates().size());
        assertTrue(size > answer.getRoutes().get(0).getGeometry().getCoordinates().size());
        assertThrows(FailedToParseException.class, () -> mapbox.parseCoordinatesRadius(null, finish, 300));
    }
}