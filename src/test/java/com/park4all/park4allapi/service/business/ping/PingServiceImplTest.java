package com.park4all.park4allapi.service.business.ping;

import com.park4all.park4allapi.Park4allApiApplication;
import com.park4all.park4allapi.entity.business.net.EnumRequestMethod;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.entity.exception.business.FailedToConnectException;
import com.park4all.park4allapi.entity.exception.business.ServiceNotAvailableException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class PingServiceImplTest {

    @Autowired
    PingService pingService;

    @Mock
    private HttpServletRequest request;

    private static Thread api;

    @BeforeAll
    static void init() throws InterruptedException {
        api = new Thread(new ApiThread());
        api.start();
        System.out.println("Api running");
        Thread.sleep(2500);
    }

    @BeforeEach
    public void initEach(){
        Mockito.when(request.getLocalPort()).thenReturn(8080);
    }

    @Test
    void testPingService() throws ApiRunTimeException {
        int expected = HttpStatus.OK.value();
        Mockito.when(request.getLocalName()).thenReturn("127.0.0.1");
        int result = pingService.sendPingRequest(request,EnumRequestMethod.GET,"/process/ping");
        assertEquals(expected,result);

        Mockito.when(request.getLocalName()).thenReturn("0:0:0:0:0:0:0:1");
        result = pingService.sendPingRequest(request,EnumRequestMethod.GET,"/process/ping");
        assertEquals(expected,result);
    }

    @Test
    void testFailedToConnectException() {
        Mockito.when(request.getLocalName()).thenReturn("199.245.15.12");
        Assertions.assertThrows(FailedToConnectException.class, () -> pingService.sendPingRequest(request,EnumRequestMethod.GET,"/"));
    }

    @Test
    void testServiceNotAvailableException() {
        Mockito.when(request.getLocalName()).thenReturn("127.0.0.1");
        Assertions.assertThrows(ServiceNotAvailableException.class, () -> pingService.sendPingRequest(request,EnumRequestMethod.GET,"/fakepath"));
    }

    @AfterAll
    static void afterAll(){
        api.interrupt();
        System.out.println("Api stopped");
    }

    static class ApiThread implements Runnable {

        @Override
        public void run() {
            Park4allApiApplication.main(new String[0]);
        }
    }
}