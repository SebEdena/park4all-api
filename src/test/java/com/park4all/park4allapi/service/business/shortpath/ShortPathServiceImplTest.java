package com.park4all.park4allapi.service.business.shortpath;

import com.park4all.park4allapi.business.distance.Distance;
import com.park4all.park4allapi.business.graph.context.GraphContext;
import com.park4all.park4allapi.business.graph.context.GraphContextImpl;
import com.park4all.park4allapi.business.graph.global.GlobalGraph;
import com.park4all.park4allapi.business.score.compute.ScoreCompute;
import com.park4all.park4allapi.business.score.compute.testClasses.MockVehicule;
import com.park4all.park4allapi.business.score.priorityParser.PriorityParser;
import com.park4all.park4allapi.business.utils.csv.ParkingParser;
import com.park4all.park4allapi.business.utils.graph.GraphUtils;
import com.park4all.park4allapi.business.vehicle.Vehicle;
import com.park4all.park4allapi.entity.business.api.process.ApiParam;
import com.park4all.park4allapi.entity.business.api.process.ApiResponse;
import com.park4all.park4allapi.entity.mapbox.Answer;
import com.park4all.park4allapi.entity.parking.bean.Geo;
import com.park4all.park4allapi.entity.parking.bean.Park;
import com.park4all.park4allapi.entity.parking.bean.Weight;
import com.park4all.park4allapi.entity.parking.fieldEnum.Type;
import com.park4all.park4allapi.entity.score.priority.Priority;
import com.park4all.park4allapi.entity.exception.business.ApiRunTimeException;
import com.park4all.park4allapi.service.business.mapbox.service.MapboxService;
import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ShortPathServiceImplTest {

    @InjectMocks
    private ShortPathServiceImpl shortPathService;

    @Autowired
    private ParkingParser parkingParserAutowired;

    @Autowired
    private GraphUtils graphUtilsAutowired;

    @Autowired
    private PriorityParser priorityParserAutowired;

    @Autowired
    private MapboxService mapboxServiceAutowired;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ScoreCompute scoreCompute;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private MapboxService mapboxService;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private GlobalGraph globalGraph;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PriorityParser priorityParser;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private Distance haversine;

    private GraphContext graphContext;
    private ApiResponse apiResponse;
    private Vehicle vehicle;
    private Map<Priority,Integer> priorityMap;

    @Value("${shortpath.parking.max}")
    private int numberEdge;

    @BeforeAll
    void init() throws IOException {
        ReflectionTestUtils.setField(shortPathService, "numberParkingMax", numberEdge);
        vehicle = new MockVehicule();
        graphContext = new GraphContextImpl();

        String[] tabPriorities = {"price", "distance"};
        priorityMap = priorityParser.prioritiesToMap(priorityParser.parsePriorities(tabPriorities));

        Map<String, Park> parkings = parkingParserAutowired.parseParkings("/csv/test/shortestpath/carparks.csv");
        List<Weight> weights = parkingParserAutowired.parseWeights("/csv/test/shortestpath/graphlinks.csv");

        Graph<Park, Weight> graph = graphUtilsAutowired.initEmptyGraph();
        graphUtilsAutowired.initGraph(graph, parkings, weights);
        graphContext.setGraph(graph);

        Geo geo = new Geo();
        geo.setLatitude(12d);
        geo.setLongitude(8d);
        initApiParam(geo);
    }

    @Test
    void testInitGeo() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Geo geo = new Geo();
        geo.setLatitude(12d);
        geo.setLongitude(8d);
        Method initPark = shortPathService.getClass().getDeclaredMethod("initPark", Geo.class, String.class);
        initPark.setAccessible(true);
        Park result = (Park)initPark.invoke(shortPathService, geo, "myId");
        Park expected = new Park();
        expected.setId("myId");
        expected.setGeo(geo);
        expected.setType(Type.TECHNICAL);

        assertEquals(geo, result.getGeo());
        assertEquals(expected.getId(),result.getId());
        assertEquals(expected.getGeo(),result.getGeo());
        assertEquals(expected.getType(),result.getType());
    }

    @Test
    void testCompute() throws ApiRunTimeException {
        Mockito.when(mapboxService.getPathFromGeos(Mockito.anyList()))
                .thenReturn(Mockito.mock(InputStream.class));
        Mockito.when(mapboxService.parseInputStreamToJava(Mockito.any(InputStream.class)))
                .thenReturn(Mockito.mock(Answer.class));
        Mockito.when(mapboxService.parseCoordinatesRadius(Mockito.any(Answer.class), Mockito.any(Geo.class), Mockito.anyInt()))
                .thenReturn(apiResponse.getParam().getStart());
        Mockito.when(globalGraph.createSubGraph(Mockito.any(Park.class), Mockito.any(Park.class), Mockito.anyInt(), Mockito.any(Vehicle.class)))
                .thenReturn(graphContext);
        Mockito.when(priorityParser.prioritiesToMap(Mockito.anyList()))
                .thenReturn(priorityMap);
        Mockito.when(scoreCompute.getCarparkScore(Mockito.any(Park.class), Mockito.any(Park.class), Mockito.any(GraphContext.class),Mockito.any(Vehicle.class), Mockito.any(Distance.class), Mockito.anyMap()))
                .thenReturn(2d);
        Mockito.when(scoreCompute.getWeightScore(Mockito.any(Weight.class), Mockito.any(GraphContext.class)))
                .thenReturn(1.5d);

        shortPathService.computePath(apiResponse);
        assertEquals(1, apiResponse.getCarparks().size());
        assertEquals("t2", apiResponse.getCarparks().get(0).getPark().getId());
    }

    @Test
    void testShortestPath() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Park start = graphContext.getGraph().vertexSet().stream()
                .filter(park -> park.getId().equals("start"))
                .collect(Collectors.toList()).get(0);
        Park finish = graphContext.getGraph().vertexSet().stream()
                .filter(park -> park.getId().equals("finish"))
                .collect(Collectors.toList()).get(0);
        Method getPath = shortPathService.getClass().getDeclaredMethod("getPath", GraphContext.class, Park.class, Park.class);
        getPath.setAccessible(true);

        List<Park> result = (List<Park>) getPath.invoke(shortPathService, graphContext,start, finish);
        assertEquals(4, result.size());

        assertEquals("start", result.get(0).getId());
        assertEquals("t2", result.get(1).getId());
        assertEquals("t4", result.get(2).getId());
        assertEquals("finish", result.get(3).getId());
    }

    private void initApiParam(Geo g){
        apiResponse = new ApiResponse();
        apiResponse.setParam(new ApiParam());
        Geo geo = new Geo();
        geo.setLatitude(g.getLatitude());
        geo.setLongitude(g.getLongitude());
        apiResponse.getParam().setStart(geo);
        geo.setLatitude(g.getLongitude());
        geo.setLongitude(g.getLatitude());
        apiResponse.getParam().setFinish(geo);
        apiResponse.getParam().setRadius(100);
        apiResponse.getParam().setVehicle(vehicle);
        String[] tabPriorities = {"price", "distance"};
        apiResponse.getParam().setPriorities(priorityParserAutowired.parsePriorities(tabPriorities));
    }
}